using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;
using Pathfinding;

[RAINAction]
public class Move : RAINAction
{
	bool pathSearched = false;
	bool destinationReached = false;
	Path path;
	Character character;


    public override void Start(RAIN.Core.AI ai)
    {
        base.Start(ai);
    	

		character = ai.Body.GetComponent<Character> ();

		float x = UnityEngine.Random.Range (-30f, 30f);
		float y = UnityEngine.Random.Range (-30f, 30f);


		character.startPath (new Vector3 (x, 0f, y), false, OnPathFound, OnDestinationReached);
	}

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        /*
       foreach (RAIN.Entities.Aspects.RAINAspect entity in ai.Senses.SenseAll()) {

       }
       */
        if (pathSearched) {
			pathSearched = false;
			if (path.error) {
				return ActionResult.FAILURE;
			}
		}

       


		if (destinationReached) {
			destinationReached = false;
			return ActionResult.SUCCESS;
		}

		return ActionResult.RUNNING;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
		//character.stop ();
        base.Stop(ai);
    }

	public void OnPathFound (Path p) {
		path = p;
		pathSearched = true;
	}

	public void OnDestinationReached() {
		destinationReached = true;
	}
}