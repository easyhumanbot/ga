using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class DecideThreat : RAINAction
{
    Character target;
    Character me;

    public override void Start(RAIN.Core.AI ai)
    {
        me = ai.Body.GetComponent<Character>();
        target = ai.WorkingMemory.GetItem<GameObject>("playerDetected").GetComponent<Character>();
        base.Start(ai);
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        Debug.Log("Decide");

        if (target.IsWeaponEquipped()) {
            me.EquipWeaponFromInventory();
            ai.WorkingMemory.SetItem("combatMode", true);
        }
        

        return ActionResult.SUCCESS;
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}