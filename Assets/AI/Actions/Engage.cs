using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RAIN.Action;
using RAIN.Core;

[RAINAction]
public class Engage : RAINAction
{

	GameObject target;

	Character me;

    public override void Start(RAIN.Core.AI ai)
    {
		target = ai.WorkingMemory.GetItem<GameObject> ("playerDetected");
        if (target != null) {
            me = ai.Body.GetComponent<Character>();

            me.shootCharacter(target);
        }
        base.Start(ai);
        
    }

    public override ActionResult Execute(RAIN.Core.AI ai)
    {
        if (target == null) {
            return ActionResult.FAILURE;
        }
        /*
		IList<RAIN.Entities.Aspects.RAINAspect> aspects = ai.Senses.Sense ("Visual", "Player", RAIN.Perception.Sensors.RAINSensor.MatchType.BEST);
        
        bool targetIsVisible = false;
        foreach (RAIN.Entities.Aspects.RAINAspect aspect in aspects) {
            if (aspect.Entity.Form == target) {
                targetIsVisible = true;
            }
        }
        */

        if (target.GetComponent<Character>().isDead) {
			return ActionResult.SUCCESS;
		}
        if (me.shotsToMake <= 0 && me.handCursor <= 0) {
            return ActionResult.SUCCESS;
        }

        if (me.targetIsVisible) {
            Debug.Log("SHOOTING!");
            return ActionResult.RUNNING;            
		} else {
            Debug.Log("CAN'T SEEE!!!");
            ai.WorkingMemory.SetItem<GameObject>("playerDetected", null);
            return ActionResult.FAILURE;
        }
		
    }

    public override void Stop(RAIN.Core.AI ai)
    {
        base.Stop(ai);
    }
}