﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UI.Inventory;

namespace UI {
    namespace Inventory {

        public interface ISlot {
            void OnEquip(IItem item, ISlot previousSlot);
            void OnUnequip(IItem item, ISlot nextSlot);
            string GetSlotType();
        }

        public class Slot : MonoBehaviour, IDropHandler, ISlot {

            public int i;
            public int j;
            private IItem item;

            public void OnDrop(PointerEventData eventData) {
                Item.dragged.transform.SetParent(transform);
                Item.dragged.transform.localPosition = new Vector3(0, 0, 0);

                item = Item.dragged.GetComponentInChildren<IItem>();

                if (item.GetSlot() == this) {
                    return;
                }
                item.GetSlot().OnUnequip(item, this);

                item.SetSlot(this);

                ((ISlot)this).OnEquip(item, item.GetSlot());
            }

            public string GetSlotType() {
                return "Slot";
            }

            public void OnEquip(IItem item, ISlot previousSlot) {
                Debug.Log("Slot equipped.");
            }

            public void OnUnequip(IItem item, ISlot nextSlot) {
                Debug.Log("Slot unequipped.");
            }
        }

    }
}