﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UI.Inventory;

namespace UI {
    namespace Inventory {

        [RequireComponent(typeof(CanvasGroup))]
        public class WeaponItem : Item, IItem {

            public new ItemInfo GetItem() {
                return itemInfo;
            }


            public void SetItem(ItemInfo item) {
                itemInfo = item;
            }
        }        
    }
}