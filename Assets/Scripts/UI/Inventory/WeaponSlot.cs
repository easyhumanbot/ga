﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UI.Inventory;

namespace UI {
    namespace Inventory {

        public class WeaponSlot : Slot, ISlot {
            public new string GetSlotType() {
                return "WeaponSlot";
            }

            public new void OnEquip(IItem item, ISlot previousSlot) {
                Equip(((WeaponItem)item), previousSlot);
            }

            public new void OnUnequip(IItem uiItem, ISlot nextSlot) {
                if (GameManager.Instance != null) {
                    GameManager.Instance.character.Unequip();
                }

                if (((Item)uiItem).itemInfo.prefab != null) Destroy(((Item)uiItem).itemInfo.prefab);
                ((Item)uiItem).itemInfo.prefab = null;
            }


            private void Equip(WeaponItem uiItem, ISlot previousSlot) {
                if (GameManager.Instance != null) {
                    
                    GameManager.Instance.character.Equip(uiItem.itemInfo);
                }

                uiItem.transform.localPosition = new Vector3(0, 25, 0);
            }
        }

    }
}