﻿using UnityEngine;
using System.Collections;
using UI.Inventory;

namespace UI {

    namespace Inventory {

        public class GroundInventory : Inventory {

            public new void LoadInventory(Game.Character character) {
                Clear();

                foreach (GameObject gameObject in ItemInfo.itemsOnGround) {
                    ItemInfo itemInfo = gameObject.GetComponent<ItemInfo>();


                    GameObject uiItem = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/UI/Inventory/Item"));
                    uiItem.transform.parent = slots[itemInfo.i, itemInfo.j].transform;
                    uiItem.transform.localPosition = Vector3.zero;


                    WeaponItem item = uiItem.AddComponent<WeaponItem>();
                    item.itemInfo = new ItemInfo();
                    item.itemInfo.CopyFrom(itemInfo);

                    //WeaponItem weap = uiItem.AddComponent<WeaponItem>();
                    //weap.item = item;
                }
            }
        }

    }
}