﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UI.Inventory;

namespace UI {
    namespace Inventory {

        

        public interface IItem {
            ISlot GetSlot();
            void SetSlot(ISlot slot);
            ItemInfo GetItem();
            void SetItem(ItemInfo itemInfo);
        }

        [RequireComponent(typeof(CanvasGroup))]
        public class Item : MonoBehaviour, IItem, IBeginDragHandler, IDragHandler, IEndDragHandler {

            public static GameObject dragged;
            public Transform startParent;
            protected ISlot slot;

            public ItemInfo itemInfo;

            public void OnBeginDrag(PointerEventData eventData) {
                GetComponent<CanvasGroup>().blocksRaycasts = false;
                dragged = gameObject;
                startParent = dragged.transform.parent;
            }

            public void OnDrag(PointerEventData eventData) {
                dragged.transform.position = Input.mousePosition;
            }

            public void OnEndDrag(PointerEventData eventData) {
                GetComponent<CanvasGroup>().blocksRaycasts = true;

            }

            void Start() {
                slot = transform.parent.GetComponent<ISlot>();
            }

            public void OnSlotEquip(ISlot slot) {
                this.slot = slot;
            }

            public ISlot GetSlot() {
                return slot;
            }

            public void SetSlot(ISlot slot) {
                this.slot = slot;
            }
            
            public ItemInfo GetItem() {
                return itemInfo;
            }

            public void SetItem(ItemInfo item) {
                itemInfo = item;
            }

        }

    }
}