﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;
using UI.Inventory;

namespace UI {
    namespace Inventory {


        public class GroundSlot : Slot, ISlot {


            public new void OnEquip(IItem uiItem, ISlot previousSlot) {
                Equip((Item)uiItem);
            }

            private void Equip(Item uiItem) {
                uiItem.itemInfo.Create();
                uiItem.itemInfo.prefab.transform.position = GameManager.Instance.character.transform.position;

                ItemInfo.itemsOnGround.Add(uiItem.itemInfo.prefab);
                Debug.Log(ItemInfo.itemsOnGround.Count);
            }

            public new void OnUnequip(IItem uiItem, ISlot previousSlot) {
                ItemInfo.itemsOnGround.Remove(((Item)uiItem).itemInfo.prefab);
                Destroy(uiItem.GetItem().prefab);
                ((Item)uiItem).itemInfo.prefab = null;
                //Destroy(((Item)uiItem).itemInfo.prefab);
            }

            public new string GetSlotType() {
                return "GroundSlot";
            }
        }

    }
}