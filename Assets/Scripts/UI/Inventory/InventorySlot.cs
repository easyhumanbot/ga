﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;
using UI.Inventory;

namespace UI {
    namespace Inventory {
        public class InventorySlot : Slot, ISlot {
            public string GetSlotType() {
                return "InventorySlot";
            }

            public void OnEquip(IItem uiItem, ISlot previousSlot) {
                uiItem.GetItem().i = i;
                uiItem.GetItem().j = j;
                MainGame.Instance.selectedGameCharacter.inventory.Add(uiItem.GetItem());                
            }

            public void OnUnequip(IItem uiItem, ISlot nextSlotm) {
                MainGame.Instance.selectedGameCharacter.inventory.Remove(uiItem.GetItem());                
            }

        }
    }
}