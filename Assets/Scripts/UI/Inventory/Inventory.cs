﻿using UnityEngine;
using System.Collections;

namespace UI {

    namespace Inventory {

        public class Inventory : MonoBehaviour {

            public int x = 5;
            public int y = 5;

            public string slotPrefab = "Prefabs/UI/Inventory/GroundSlot";

            public Slot[,] slots;

            void Start() {
                slots = new Slot[x, y];

                for(int i = 0; i < 5; i++) {
                    for(int j = 0; j < 5; j++) {
                        GameObject slot = (GameObject)UnityEngine.Object.Instantiate(Resources.Load(slotPrefab));
                        slot.GetComponent<Slot>().i = i;
                        slot.GetComponent<Slot>().j = j;
                        slot.transform.parent = transform;

                        slots[i, j] = slot.GetComponent<Slot>();
                    }
                }
                
            }
            
            protected void Clear() {
                for (int i = 0; i < 5; i++) {
                    for (int j = 0; j < 5; j++) {
                        Item child = slots[i, j].GetComponentInChildren<Item>();
                        if (child != null) Destroy(child.gameObject);
                    }
                }
            }

            public void LoadInventory(Game.Character character) {
                Clear();                

                foreach (ItemInfo item in character.inventory) {
                    GameObject uiItem = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/UI/Inventory/Item"));
                    uiItem.transform.parent = slots[item.i, item.j].transform;
                    uiItem.transform.localPosition = Vector3.zero;


                    WeaponItem weap = uiItem.AddComponent<WeaponItem>();
                    weap.SetItem(item);
                    
                }
            }

            protected void AddItem(ItemInfo item) {
                
            }

        }
    }

}