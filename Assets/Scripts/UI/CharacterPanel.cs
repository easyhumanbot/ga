﻿using UnityEngine;
using System.Collections;

public class CharacterPanel : MonoBehaviour {

    public UI.Inventory.GroundInventory groundInventory;
    public UI.Inventory.Inventory characterInventory;
    

	public void LoadInventory(Game.Character character) {
        groundInventory.LoadInventory(character);
        characterInventory.LoadInventory(character);
    }
}
