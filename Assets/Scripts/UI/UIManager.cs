﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Game;

namespace UI {

	
	public class UIManager : MonoBehaviour {

        public static UIManager Instance;

        public GameObject characterList;
		public GameObject groupList;
        public GameObject canvas;
        public GameObject listSelectAction;
        public UI.Inventory.Inventory inventory;
        public UI.Inventory.Inventory groundInventory;

        public Text date;
        public Text money;

        public Game.Game game;

        void Start () {
            game = MainGame.Instance;
            if (UIManager.Instance == null) {
                UIManager.Instance = this;
            }

            listSelectAction.SetActive(false);

            refresh();
		}

		public void refresh() {
            characterList.GetComponent<UI.CharacterList>().refresh();
            groupList.GetComponent<UI.GroupList>().refresh();

            date.text = game.date.ToString();
            money.text = game.lcs.money.ToString();
        }


        public void nextDay() {
            MainGame.Instance.nextDay();

            refresh();
        }


        public void SetCharacterAction(string actionName) {
            if (CharacterOption.selectedOption != null) {
                IAction action = (IAction)System.Activator.CreateInstance(System.Type.GetType(actionName));
                CharacterOption.selectedOption.character.setAction(action);
                CharacterOption.selectedOption.Refresh();

                listSelectAction.SetActive(false);
            }
        }


        public void ShowInventory(bool show) {
            if (show == true) {
                inventory.gameObject.transform.parent.gameObject.SetActive(true);
                groundInventory.gameObject.SetActive(true);
            } else {
                inventory.gameObject.transform.parent.gameObject.SetActive(false);
                groundInventory.gameObject.SetActive(false);

                MainGame.Instance.selectedGameCharacter = null;
            }
        }

        public void ShowInventory(Game.Character character) {
            //If selecting the same character then we hide 
            if (MainGame.Instance.selectedGameCharacter == character) {
                ShowInventory(false);
            } else {
                MainGame.Instance.selectedGameCharacter = character;
                inventory.LoadInventory(character);

                ShowInventory(true);
            }
        }
    }

}