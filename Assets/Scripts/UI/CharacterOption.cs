﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

namespace UI {

    public interface IOption {
        void Refresh();
        void SetCharacter(Game.Character character);
    }

    public class CharacterOption : MonoBehaviour, IOption {

        public Text actionText;

        public Button buttonAddToGroup;
        public Button buttonAction;
        public Button buttonInventory;

        public Game.Character character;
        public static CharacterOption selectedOption;

        public void Start() {

        }

        public void SetCharacter(Game.Character character) {
            this.character = character;
            Refresh();
        }

        public void Refresh() {
            buttonAddToGroup.onClick.RemoveAllListeners();
            buttonAddToGroup.onClick.RemoveAllListeners();
            buttonInventory.onClick.RemoveAllListeners();

            buttonAddToGroup.onClick.AddListener(() => addCharacterToGroup(character));
            buttonAction.onClick.AddListener(() => SelectAction(character));
            buttonInventory.onClick.AddListener(() => InventoryAction(character));

            if (character.actions.Count > 0) {
                actionText.text = character.actions[0].getTitle();
            }
        }



        public void addCharacterToGroup(Game.Character character) {
            if (MainGame.Instance.lcs.getGroups()[0].isMember(character))
                return;

            MainGame.Instance.lcs.getGroups()[0].addMember(character);
            UI.UIManager.Instance.refresh();
        }

        public void SelectAction(Game.Character character) {
            selectedOption = this;
            UIManager.Instance.listSelectAction.SetActive(true);
        }

        public void InventoryAction(Game.Character character) {
            UIManager.Instance.ShowInventory(character);
        }

    }

}