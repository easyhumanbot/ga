﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace UI {

    public class CharacterInGroupList : UI.Widgets.List {

        public Game.Group group;

        void Start() {

        }

        void Update() {

        }

        public void SetGroup(Game.Group group) {
            this.group = group; 
            foreach (Game.Character character in group.getMembers()) {
                AddItem(character);
            }
        }

        public void AddItem(Game.Character character) {
            GameObject item = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/UI/CharacterInGroupOption"));
            item.transform.parent = transform.FindChild("List").transform;

            item.transform.FindChild("TextName").GetComponent<Text>().text = character.name;

            Button buttonRemove = item.transform.FindChild("ButtonRemove").GetComponent<Button>();
            buttonRemove.onClick.AddListener(() => RemoveCharacter(character));
        }

        
        public void Refresh() {
            clear();
            SetGroup(group);
        }

        public void RemoveCharacter(Game.Character character) {
            group.RemoveMember(character);
            Refresh();
        }
    }

}