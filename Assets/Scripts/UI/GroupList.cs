﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace UI {

    public class GroupList : UI.Widgets.List {

        void Start() {

        }

        void Update() {

        }

        void addItem(Game.Group group) {
            GameObject item = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/UI/CharacterInGroupList"));
            item.transform.parent = transform.FindChild("List").transform;

            item.GetComponent<UI.CharacterInGroupList>().SetGroup(group);          
        }

        

        public void refresh() {
            clear();

            foreach (Game.Group group in MainGame.Instance.lcs.getGroups()) {
                addItem(group);
            }
        }
    }

}