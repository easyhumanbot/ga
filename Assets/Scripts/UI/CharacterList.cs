﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UI;

namespace UI {

    public class CharacterList : UI.Widgets.List {
        

        public void addItem(Game.Character character) {
            GameObject item = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/UI/CharacterOption"));
            item.transform.parent = transform.FindChild("List").transform;

            item.transform.FindChild("TextName").GetComponent<Text>().text = character.name;
            

            item.GetComponent<IOption>().SetCharacter(character);
        }

        public void refresh() {
            clear();

            foreach (Game.Character character in MainGame.Instance.lcs.getMembers()) {
                addItem(character);
            }
            
        }

        
    }

}