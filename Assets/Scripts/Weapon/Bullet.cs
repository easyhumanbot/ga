﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bullet : MonoBehaviour {

	public GameObject bulletHolePrefab;

	public Vector3 direction;



	void OnCollisionEnter(Collision collision) {
        //Debug.Log (collision.gameObject.name);
        if (CharacterUtil.getLimbNames().Contains(collision.gameObject.name)) {
            collision.gameObject.GetComponentInParent<Character>().die();
            Destroy(gameObject);
            return;
        } else {

            foreach (ContactPoint hit in collision.contacts) {
                float angle = Vector3.Angle(direction, hit.normal);


                Vector3 bulletHolePosition = hit.point + hit.normal * 0.01f;

                Quaternion bulletHoleRotation = Quaternion.FromToRotation(-Vector3.forward, hit.normal);

                GameObject hole = (GameObject)GameObject.Instantiate(bulletHolePrefab, bulletHolePosition, bulletHoleRotation);

                hole.transform.parent = collision.gameObject.transform;
            }
            Destroy(gameObject);
        }

		if (collision.relativeVelocity.magnitude > 2) {
			
		}




	}


	void makeHole(ContactPoint hit) {
		
	}
}
