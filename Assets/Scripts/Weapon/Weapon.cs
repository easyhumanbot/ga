﻿using UnityEngine;
using System.Collections;

public class Weapon : MonoBehaviour {
	public Rigidbody projectilePrefab;
	public GameObject gun;

	public float bulletStartSpeed = 400f;
	//private Rigidbody instantiatedProjectile;
	public float wait = 0f;
	public float fireTime = 0.3f;

	public int clipSize = 30;

	public void fire() {
		makeShot ();
	}

	private void makeShot() {
		if (wait > 0) {
			return;
		}
		if (clipSize == 0) {
			return;	
		}

		clipSize -= 1;
		wait = fireTime;



		AudioSource shotSound = GetComponent<AudioSource> ();
		AudioSource audio = gameObject.AddComponent<AudioSource >();
		audio.clip = shotSound.clip;
		audio.volume = shotSound.volume;
		audio.Play ();
		Destroy (audio, audio.clip.length);

		Rigidbody instantiatedProjectile = Instantiate(projectilePrefab,
			gun.transform.position,
			gun.transform.rotation)
			as Rigidbody;
		instantiatedProjectile.GetComponent<Bullet>().direction = gun.transform.TransformDirection(new Vector3(0, -1, 0));
		instantiatedProjectile.AddForce(gun.transform.TransformDirection(new Vector3(0, -1, 0) * bulletStartSpeed));

		GetComponentInChildren<ParticleSystem> ().Emit (1);
	}

	public void Update() {
		wait -= Time.deltaTime;
		
		//if (instantiatedProjectile != null) {
			//Debug.Log(instantiatedProjectile.GetComponent<Rigidbody>().velocity.magnitude);
		//}
	}
}
