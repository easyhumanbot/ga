﻿using UnityEngine;
using System.Collections;


public interface IInteractableObject {
    void StartInteraction(Character character);
}

public class Computer : MonoBehaviour, IInteractableObject {
    
	void Start () {
	
	}
	
	void Update () {
	
	}

    public void StartInteraction(Character character) {
        GameManager.Instance.dialogueManager.startDialogue(typeof(Dialogues.ComputerTalk), character, gameObject);
    }

}
