﻿using UnityEngine;
using System.Collections;
using Pathfinding;
using RAIN.Core;

public class GameManager : MonoBehaviour {

    public CharacterPanel characterPanel;

    public GameObject radialMenu;
    public GameObject aboveText;

    public GameObject characterObj;
	public DialogueManager dialogueManager;

	public bool altPressed = false;
	public bool ctrlPressed = false;

	public Character character;
    Character targetCharacter;
    IInteractableObject interactableObject;

    public static GameManager Instance;

    public Texture2D handCursor;

    void Start () {
        Vector3 startPosition = new Vector3(0, 0, -5);
        foreach (Game.Character gameCharacter in MainGame.Instance.lcs.getGroups()[0].getMembers()) {
            Character ch = CreatePlayer(startPosition);
            ch.Create(gameCharacter);

            startPosition += new Vector3(0, 0, -1);

            SelectCharacter(ch);
        }

        GameManager.Instance = this;
        
    }

    public Character CreatePlayer(Vector3 position) {
        GameObject obj = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Character/Character"));
        obj.transform.position = position;
        obj.GetComponent<Character>().characterLoadedCallback = CharacterLoadedCallback;


        return obj.GetComponent<Character>();
    }

    void CharacterLoadedCallback(Character character) {
        character.ControlByPlayer();
        character.defaultAimTime = 0.5f;
        character.SetVisible(true);
    }


    public Character CreateCharacter(Vector3 position) {
        GameObject obj = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Character/NPC"));
        obj.transform.position = position;
        return obj.GetComponent<Character>();
    }

    void Update () {

        

        if (Input.GetButtonDown("Alt")) {
			altPressed = true;
		} else if (Input.GetButtonUp("Alt")) {
			altPressed = false;
		}

		if (Input.GetButtonDown("Ctrl")) {
			ctrlPressed = true;
		} else if (Input.GetButtonUp("Ctrl")) {
			ctrlPressed = false;
		}


		if (Input.GetButtonDown ("ToggleWeapon")) {
			character.toggleWeapon();
		}

		if (Input.GetButtonDown ("Crouch")) {
			character.crouch();
		}
		if (Input.GetButtonDown ("Stand")) {
			character.stand();
		}
		if (Input.GetButton ("Interact")) {
			character.interact ();
		}

		if (Input.GetButtonDown ("Pause")) {
            if (Time.timeScale < 1) {
                Unpause();
            } else {
                Pause();
            }
		}

        if (Input.GetKeyDown(KeyCode.R)) {
            character.reload();
        }

        if (Input.GetKeyDown(KeyCode.I)) {
            characterPanel.gameObject.active = !characterPanel.gameObject.active;
            if (characterPanel.gameObject.active) {
                characterPanel.LoadInventory(character.gameCharacter);
            }
        }





        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        bool raycasted = Physics.Raycast(ray, out hit, 1000);

        if (raycasted) {
            targetCharacter = hit.collider.GetComponent<Character>();
            interactableObject = hit.collider.GetComponent<IInteractableObject>();
            SetCursor();
            
            if (Input.GetMouseButtonDown(0) && targetCharacter != null) {
                SelectCharacter(targetCharacter);
                return;
            }

            if (Input.GetMouseButtonDown(1)) {
                if (ctrlPressed && targetCharacter != null) {
                    character.shootCharacter(hit.collider.gameObject);
                    return;
                } else if (ctrlPressed) {
                    character.shoot(hit.point);
                    return;
                } else if (altPressed) {
                    character.startPath(hit.point, true);
                    return;
                } else if (targetCharacter) {
                    StartDialogue();
                    return;
                } else if (interactableObject != null) {
                    interactableObject.StartInteraction(character);
                } else {
                    character.startPath(hit.point);
                    return;
                }
            }

            
        }

	}

    void StartDialogue() {
        if (targetCharacter.gameCharacter.role == "bankTeller") {
            dialogueManager.startDialogue(typeof(Dialogues.BankTellerTalk), character, targetCharacter);
        } else {
            dialogueManager.startDialogue(typeof(Dialogues.NPCTalk), character, targetCharacter);
        }
    }

    void SetCursor() {
        if (targetCharacter != null) {
            Cursor.SetCursor(handCursor, new Vector2(0, 0), CursorMode.ForceSoftware);
        } else {
            Cursor.SetCursor(null, new Vector2(0, 0), CursorMode.ForceSoftware);
        }
    }

    public void SelectCharacter(Character ch) {
        character = ch;
        MainGame.Instance.selectedGameCharacter = ch.gameCharacter;

        character.Say("Let's go.");
    }

	public void onDisable() {
	}

    public bool allowUnpause = true;
    public void Pause() {
        Time.timeScale = 0.0f;
    }


    public void Unpause() {
        if (allowUnpause) {
            Time.timeScale = 1;
        }
    }

    public void HardPause() {
        allowUnpause = false;
        Time.timeScale = 0f;
    }

    public void HardUnpause() {
        allowUnpause = true;
        Time.timeScale = 1f;
    }
}
