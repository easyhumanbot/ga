﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using Game;
using Dialogue;

namespace Dialogues {

    public class MeetingTalk : PlayerPhrase, IPhrase {

        public MeetingTalk() {
            msg = DialogueManager.Instance.gameCharacter.getName() + " talks with " + DialogueManager.Instance.gameNpc.getName();
            options.Add(new Recruit());
        }

        public class Recruit : Option, IOption {
            public Recruit() {
                text = "Try to recruit.";
                phrase = new RecruitPhrase();
            }

            public void run() {
                DialogueManager.Instance.gameNpc.boss = DialogueManager.Instance.gameCharacter;
                MainGame.Instance.lcs.addMember(DialogueManager.Instance.gameNpc);
                DialogueManager.Instance.endDialogue();

                UI.UIManager.Instance.refresh();
            }

            public class RecruitPhrase : Phrase, IPhrase {
                public RecruitPhrase() {
                    msg = DialogueManager.Instance.gameCharacter.getName() + " talks about corporates using child labor.";
                    options.Add(new Accept());
                }

                public class Accept : Option, IOption {
                    public Accept() {
                        text = DialogueManager.Instance.gameNpc.getName() + " accepts to join to lcs.";
                    }

                    public void run() {
                        MainGame.Instance.lcs.addMember(DialogueManager.Instance.gameNpc);
                        DialogueManager.Instance.endDialogue();
                    }
                    
                }
            }

            
        }
        
    }

    public class ComputerTalk : PlayerPhrase, IPhrase {
        public ComputerTalk() {
            msg = "Access denied. Hack into the system?";
            options.Add(new Yes());
        }

        public class Yes : Option, IOption {
            public Yes() {
                text = "Yes";
            }

            public void run() {
                DialogueManager.Instance.endDialogue();
            }
        }
    }

    public class BankTellerTalk : PlayerPhrase, IPhrase {

        public BankTellerTalk() {
            this.msg = "Bank teller silently wathes on you.";
            this.options.Add(new Rob());
        }

        public class Rob : Option, IOption {
            public Rob() {
                this.text = "Rob the bank.";
            }

            public void run() {
                DialogueManager.Instance.character.Say("All on the ground! It's a robbery!");
                DialogueManager.Instance.endDialogue();
                
            }

            public bool runInstantly() {
                return true;
            }
        }
    }

    public class NPCTalk : PlayerPhrase, IPhrase {
        Game.Character player;
        Game.Character npc;

        public NPCTalk() {
            this.msg = "Ivy talks to Socialite";
            this.options.Add(new Politics());
            this.options.Add(new Pickup());
            this.options.Add(new CloseDialog());
        }

        public class CloseDialog : Option, IOption {
            public CloseDialog() {
                this.text = "On second thought, don't say anything.";
            }

            public void run() {
                DialogueManager.Instance.endDialogue();
            }
        }

        public class Pickup : Option {
            public Pickup() {
                this.text = "Drop a pickup line.";
                this.phrase = new PickupPhrase();
            }

            public class PickupPhrase : PlayerPhrase {
                public PickupPhrase() {
                    this.msg = "I have a genital rash. Will you rub this ointment on me?";
                    this.options.Add(new No());
                }

                public class No : Option, IOption {
                    public No() {
                        this.text = "Hahahahaha! <shakes head>";
                    }

                    public void run() {
                        DialogueManager.Instance.endDialogue();
                    }
                }
            }
        }

        public class Politics : Option {
            public Politics() {
                this.text = "Strike conversation about politics.";
                this.phrase = new SomethingDisturbing();
            }

            public class SomethingDisturbing : PlayerPhrase, IPhrase {
                public SomethingDisturbing() {
                    this.msg = "Do you want to hear something disturbing?";
                    this.options.Add(new Yes());
                    this.options.Add(new No());
                }

                public int choose() {
                    return Random.RandomRange(0, 1);
                }

                public class Yes : Option, IOption {
                    public Yes() {
                        this.text = "What?";
                        this.phrase = new Response1();
                    }

                    public bool runInstantly() {
                        return true;
                    }

                    public class Response1 : PlayerPhrase {
                        public Response1() {
                            this.msg = "Corporate executives use giant corporations as a means to become parasites that suck wealth out of this country and put it into their pockets.";
                            this.options.Add(new Yes());
                        }

                        public class Yes : Option, IOption {
                            public Yes() {
                                this.text = "That's frightening! What can we do?";
                                this.phrase = new Response1();
                            }

                            public bool runInstantly() {
                                return true;
                            }

                            public class Response1 : PlayerPhrase, IPhrase {
                                public Response1() {
                                    this.msg = "After more discussion, Socialite agreeds to come by later tonight.";
                                    this.options.Add(new CloseDialog());
                                }

                                public class CloseDialog : Option, IOption {
                                    public CloseDialog() {
                                        this.text = "";
                                    }

                                    public void run() {
                                        Debug.Log("NEW meeting!!!");
                                        Debug.Log(DialogueManager.Instance.gameCharacter.getName());
                                        DialogueManager.Instance.gameCharacter.meetings.Add(new Game.CharacterMeeting(DialogueManager.Instance.gameNpc));
                                        DialogueManager.Instance.endDialogue();
                                    }

                                    public bool runInstantly() {
                                        return true;
                                    }
                                    
                                }
                            }
                        }
                    }
                }

                public class No : Option, IOption {
                    public No() {
                        this.text = "Nah.";
                    }

                    public void run() {
                        DialogueManager.Instance.endDialogue();
                    }
                }
            }
        }
    }

}

namespace Dialogue {

    public interface IPhrase {
        string getText(bool withOptions = true);
        bool isPlayer();
        IPhrase select(int index);
        Option getOption(int index);
        int choose();

    }

    public interface IOption {
        string getText();
        void run();
        bool runInstantly();
    }

    public class Phrase : IPhrase {
        protected string msg;
        protected List<Option> options;
        public Phrase() {
            this.options = new List<Option>();
        }

        public Phrase(string msg) {
            this.msg = msg;
            this.options = new List<Option>();
        }

        public bool isPlayer() {
            return false;
        }

        public string getText(bool withOptions = true) {
            string text = msg + "\n";
            if (withOptions) {
                int c = 1;
                foreach (Option option in options) {
                    text += c.ToString() + " - " + option.text + "\n";

                    c++;
                }
            }
            return text;
        }

        public IPhrase select(int index) {
            return options[index].phrase;
        }

        public int choose() {
            return 0;
        }

        public Option getOption(int index) {
            return options[index];
        }
    }

    public class PlayerPhrase : Phrase, IPhrase {
        public bool isPlayer() {
            return true;
        }
    }

    public class Option : IOption {
        public string text = "";
        public Phrase phrase;
        public Option() {
        }

        public string getText() {
            Debug.Log("option text");
            return text;
        }

        public bool runInstantly() {
            return false;
        }

        public void run() {

        }
    }    
}


public class DialogueManager : MonoBehaviour {
    public GameObject panel;
    public Text text;
    public Character character;
    public Character npc;
    public Game.Character gameCharacter;
    public Game.Character gameNpc;
    public GameObject interactionObject;
    private bool waitForSelect = false;
    private bool waitForNPCAnswer = false;
    private bool waitForEnter = false;
    private bool runOption = false;
    int choose = 0;
    private IPhrase talk;


    public static DialogueManager Instance;

    void Awake() {
        panel.SetActive(false);

        DialogueManager.Instance = this;
    }
    

    void Update() {
        if (talk == null)
            return;

        if (runOption) {
            if (Input.GetKeyDown(KeyCode.Space)) {
                runOption = false;

                IOption option = talk.getOption(choose);
                option.run();
                choose = 0;
            }
            return;
        }

        if (waitForEnter && Input.GetKeyDown(KeyCode.Space)) {
            nextPhrase(talk.choose());
            waitForEnter = false;
        } else if (waitForNPCAnswer && Input.GetKeyDown(KeyCode.Space)) {
            choose = talk.choose();
            IOption option = talk.getOption(choose);
            display(option.getText());

            if (option.runInstantly() == true) {
                option.run();
            } else {
                runOption = true;
            }
            waitForEnter = true;
            waitForNPCAnswer = false;
        } else if (waitForSelect) {
            int key = -1;
            if (Input.GetKeyDown(KeyCode.Alpha1)) {
                key = 0;
            }
            if (Input.GetKeyDown(KeyCode.Alpha2)) {
                key = 1;
            }
            if (Input.GetKeyDown(KeyCode.Alpha3)) {
                key = 2;
            }

            if (key != -1) {
                IOption option = talk.getOption(key);
                option.run();

                if (talk != null) {
                    nextPhrase(key);
                    waitForSelect = false;
                }
            }
        }
    }


    private string say(string phrase) {
        return "Ivy Colins says, \n\"" + phrase + "\"";
    }

    void nextPhrase(int key) {
        talk = talk.select(key);
        if (!talk.isPlayer()) {
            display(talk.getText());
            waitForSelect = true;
        } else {
            display(talk.getText(false));
            waitForNPCAnswer = true;
        }
    }

    public void startDialogue(System.Type talk, Game.Character gameCharacter, Game.Character gameNpc) {
        if (GameManager.Instance != null) {
            GameManager.Instance.HardPause();
        }

        DialogueManager.Instance.gameCharacter = gameCharacter;
        DialogueManager.Instance.gameNpc = gameNpc;

        this.talk = (IPhrase)System.Activator.CreateInstance(talk);

        waitForSelect = false;
        waitForNPCAnswer = false;
        waitForEnter = false;
        runOption = false;

        display(this.talk.getText());
        waitForSelect = true;

        panel.SetActive(true);
    }

    public void startDialogue(System.Type talk, Character character, GameObject interactionObject) {
        this.character = character;
        this.npc = null;
        this.interactionObject = interactionObject;

        startDialogue(talk, character.gameCharacter, null);
    }

    public void startDialogue(System.Type talk, Character character, Character npc) {
        this.character = character;
        this.npc = npc;
        startDialogue(talk, character.gameCharacter, npc.gameCharacter);
    }

    public void endDialogue() {
        panel.SetActive(false);
        talk = null;

        if (GameManager.Instance != null) {
            GameManager.Instance.HardUnpause();
        }
    }

    public void display(string msg) {
        text.text = msg;
    }
}