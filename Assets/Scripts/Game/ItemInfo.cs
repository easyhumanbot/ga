﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemInfo : MonoBehaviour {
    public static List<GameObject> itemsOnGround = new List<GameObject>();

    public GameObject prefab;
    public string name;
    public int i = 0;
    public int j = 0;

    public string resourse = "Prefabs/Weapon/AK-47";

    public ItemInfo(string name = "") {
        this.name = name;
    }

    public void CopyFrom(ItemInfo c) {
        name = c.name;
        prefab = c.prefab;
        i = c.i;
        j = c.j;
    }

    public void Create() {
        if (prefab != null) return;

        prefab = (GameObject)UnityEngine.Object.Instantiate(Resources.Load(resourse));
        ItemInfo itemInfo = prefab.AddComponent<ItemInfo>();
        itemInfo.CopyFrom(this);
        //prefab.GetComponent<ItemInfo>().prefab = prefab;
    }
}
