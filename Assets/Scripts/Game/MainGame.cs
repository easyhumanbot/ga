﻿using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;
using System;
using System.Reflection;
using UnityEngine;
using UnityEngine.SceneManagement;
using UI;


public class MainGame : MonoBehaviour {

	#region Singleton
	private class Singleton
	{
		static Singleton()
		{
			if (instance == null)
			{
				instance = new Game.Game();
			}
		}

		internal static readonly Game.Game instance;
	}

	public static Game.Game Instance { get { return Singleton.instance; } }
	#endregion


	public void Awake() {
        
	}

	public void Update() {
		foreach(KeyCode kcode in Enum.GetValues(typeof(KeyCode))) {
			if (Input.GetKeyDown (kcode)) {
				Debug.Log ("KeyCode down: " + kcode);
			}
		}

	}

	public void loadLevel(string name) {
        SceneManager.LoadScene(name);
	}


    
}

namespace Game {

	public static class u {
		public static float max(float a, float b) {
			if (a > b)
				return a;
			return b;
		}

		public static float min(float a, float b) {
			if (a < b)
				return a;
			return b;
		}

		public static void log(string msg) {
			var logmsg = msg;
			Debug.Log(logmsg);
		}
	}

	public class Laws {
		class LawModificators {
			public LawModificators() {

			}
		}

		public interface Law {

		}

		public GunLaw gunLaw;
		public ChildLaborLaw childLaborLaw;
		public FreeSpeechLaw freeSpeechLaw;

		public Laws() {
			this.childLaborLaw = new ChildLaborLaw();
		}

		public class FreeSpeechLaw { 
			private int currentStage;

			public FreeSpeechLaw() {
				
			}
		}

		public class ChildLaborLaw : Law {
			private int currentStage;

			public ChildLaborLaw() {

			}


		}

		public class GunLaw {
			private int currentStage;

			LawModificators lawModificators;

			public GunLaw() {
				new LawModificators();

			}
		}
	}

    

	public class Game {
        public Character selectedGameCharacter;

        public float economylIndex = 1f;
		public float salaryIndex = 0.938f;
		public float productCostIndex = 1.045f;

		public Laws laws = new Laws();
		public Squad lcs = new Squad ();

        public DateTime date = new DateTime(1980, 1, 1);

		List<Character> pC = new List<Character>();

        public string[] firstNames;
        public string[] maleNames;
        public string[] femaleNames;       

        public Game() {
			Character anna = new Character ("Anna Maria");
			anna.stats.liberty.value = 10f;
			anna.setAction (new ProstituteAction ());

            ItemInfo ak47 = new ItemInfo("AK-47");
            anna.inventory.Add(ak47);
			lcs.addMember (anna);
		
			Character peter = new Character ("Peter");
            peter.setAction(new ProstituteAction());
			lcs.addMember (peter);


			Group group = new Group ("Alpha");
            group.addMember(anna);
                            
			lcs.addGroup (group);

            firstNames = Regex.Split(System.IO.File.ReadAllText(Application.dataPath + "/Resources/Text/Names/first.txt"), "\r\n|\r|\n");
            maleNames = Regex.Split(System.IO.File.ReadAllText(Application.dataPath + "/Resources/Text/Names/male.txt"), "\r\n|\r|\n");
            femaleNames = Regex.Split(System.IO.File.ReadAllText(Application.dataPath + "/Resources/Text/Names/female.txt"), "\r\n|\r|\n");            

            
        }


		public void nextDay() {
			Debug.Log ("Turn");


            foreach(Character character in lcs.getMembers()) {
                for (int i = character.meetings.Count - 1; i >= 0; i--) {
                    DialogueManager.Instance.startDialogue(typeof(Dialogues.MeetingTalk), character, character.meetings[i].with);
                    character.meetings.RemoveAt(i);
                }
            }


            foreach (Character character in lcs.getMembers()) {
                character.executeActions();
            }


            date = date.AddDays(1);
        }

		public Squad getSquad(string name) {
			return lcs;
		}
			
	}


	public class Group {
		public string name;
		public List<Character> members = new List<Character>();

		public Group(string name) {
			this.name = name;
		}

        public void addMember(Character character) {
            members.Add(character);
        }

        public void RemoveMember(Character character) {
            bool result = members.Remove(character);
            if (result == false) {
                throw new Exception("No member in group!");
            }
        }

        public bool isMember(Character character) {
            if (members.IndexOf(character) != -1) return true;
            return false;
        }

        public List<Character> getMembers() {
            return members;
        }
	}

	public class Squad {
		List<Group> groups = new List<Group>();
		List<Character> characters = new List<Character> ();
		public float money = 0.0f;


		public Squad() {
			
		}

		public List<Group> getGroups() {
			return this.groups;
		}

		public void addGroup(Group group) {
			groups.Add (group);
		}

		public void removeGroup() {
			
		}


		public List<Character> getMembers() {
			return this.characters;
		}

		public void addMember(Character character) {
			if (character.getSquad() != null) {
				throw new UnityException ("Character is already in squad! Unassing it from squad before adding to other!");
			}

			character.setSquad (this);
			characters.Add (character);
		}

		public void removeMember() {
			
		}
        
	}

	public class Mission {
		public int type;
		public string name;
		public string description;
	}

	public interface IAction {
		string getTitle();
		void executeBy(Character character);
	}

    public class Action : IAction {
        public static List<IAction> actions = new List<IAction>();

        public Action() {
        }

        public string getTitle() {
            return "";
        }

        public void executeBy(Character character) {

        }
    }



	public class IdleAction : Action, IAction {
		public IdleAction() {
		
		}

		public string getTitle() {
			return "Idle";
		}

		public void executeBy(Character character) {
			
		}
	}

	public class StealCarAction : Action, IAction {
		public StealCarAction() {
			
		}

		public string getTitle() {
			return "Idle";
		}

		public void executeBy(Character character) {
			
		}
	}

	public class IntelAction : Action, IAction {
		public IntelAction() {
		}

		public string getTitle() {
			return "Idle";
		}

		public void executeBy(Character character) {
		
		}
	}

	public class SellBrowniesAction : Action, IAction {
		public SellBrowniesAction() {
		
		}

		public string getTitle() {
			return "Sell brownies";
		}

		public void executeBy(Character character) {
			
		}
	}

    public class FraudCardsAction : Action, IAction {
        public FraudCardsAction() {

        }

        public string getTitle() {
            return "Fraud cards";
        }

        public void executeBy(Character character) {
            var earned = character.skills.hacking.value * 20f;
            character.getSquad().money += earned;

            Debug.Log(character.getName() + " earned " + earned.ToString() + "$ by frauding cards.");
        }
    }

	public class ProstituteAction : Action, IAction {
		public ProstituteAction() {
			
		}

		public string getTitle() {
			return "Prostitute";
		}

		public void executeBy(Character character) {
			float totalEarned = 100f * MainGame.Instance.salaryIndex;

			float familyNeeds = character.family.calcMoneyForTurn ();
			float corruptionPercent = (15f - u.min (character.stats.liberty.value, 15f)) / 15f;
			float corruptionNeeds = corruptionPercent * totalEarned;

			float ownSavings = corruptionNeeds;
			float squadSavings = totalEarned - ownSavings;

			character.family.money += ownSavings;

			character.getSquad ().money += squadSavings;

            Debug.Log(character.getName() + " earned " + squadSavings.ToString() + "$ by prostituting on streets.");
		}
	}



	public class Family {
		public Character father;
		public Character mother;
		public List<Character> brothers;
		public Character wife;
		public List<Character> childs;

		public float money;

		public Family() {
		
		}

		public Character getGrandFather() {
			return null;
		}

		public Character getGrandMother() {
			return null;
		}

		public float calcMoneyForTurn() {
			return 50f * MainGame.Instance.productCostIndex;
		}
	}


    public class CharacterMeeting {
        public Character with;
        public CharacterMeeting(Character with) {
            this.with = with;
        }
    }


    public class Inventory : List<ItemInfo> {
        public Inventory() : base() {

        }
    }

	public class Character {
        public Inventory inventory = new Inventory();

        public Character boss;
        public int sex = 0;
		public string name;
        public string role;
        public Stats stats;
        public Skills skills;
		public Equipment equipment;
		private Squad squad;

        public List<CharacterMeeting> meetings = new List<CharacterMeeting>();
        

		public Family family; 

		public List<IAction> actions = new List<IAction>();

		public Character(string name = "", string role = "") {
			this.name = name;
            if (this.name == "") {                
                this.name = Lower(MainGame.Instance.femaleNames.SelectRandom());
                this.name += " " + Lower(MainGame.Instance.firstNames.SelectRandom());
            }

            this.sex = UnityEngine.Random.Range(0, 2);

            this.role = role;
            
			this.stats = new Stats ();
            this.skills = new Skills();
			this.equipment = new Equipment ();

			this.family = new Family ();

            inventory.Add(new ItemInfo("AK-47"));
		}

        private string Lower(string s) {
            return s.Substring(0, 1) + s.Substring(1).ToLower();
        }


		public IAction getAction() {
			if (actions.Count > 0) {
				return actions [0];
			} 
			return new IdleAction ();
		}

		public void setAction(IAction action) {
			if (actions.Count == 1) {
				actions [0] = action;
			} else {
				addAction (action);
			}
		}

		private void addAction(IAction action) {
			if (actions.Count > 0) {
				throw new UnityException ("Only one action for character allowed.");
			}

			actions.Add (action);
		}

		public void executeActions() {
			foreach (IAction action in actions) {
				action.executeBy (this);
			}
		}

		public string getName() {
			return this.name;
		}

		public void setSquad(Squad squad) {
			if (this.squad != null) {
				throw new UnityException ("Character is already in squad! Unassing it from squad before adding to other!");
			}

			this.squad = squad;
		}

		public Squad getSquad() {
			return this.squad;
		}

		

		public class Equipment {

			public class Armor {
			
			}

			public Armor armor;

			public Equipment() {
				
			}

		}

        public class Stat {
            public string name;
            public float value;



            public Stat(string name, float value = 0.0f) {
                this.name = name;
                this.value = value;
            }


            public void setValue(float value) {
                this.value = value;
            }

            public float getValue() {
                return this.value;
            }

            public void inc(float value) {
                this.value += value;
            }
        }

        public class Stats {

			public Stat liberty = new Stat("Liberty");
			public Stat strength = new Stat("Strength");
			public Stat agility = new Stat("Agility");
			public Stat intelligence = new Stat("Intelligence");
			public Stat charisma = new Stat("Charisma");
			public Stat seduction = new Stat ("Seduction");


			public Stats() {
			}
		}

        public class Skill {
            public string name;
            public float value;



            public Skill(string name, float value = 0.0f) {
                this.name = name;
                this.value = value;
            }


            public void setValue(float value) {
                this.value = value;
            }

            public float getValue() {
                return this.value;
            }

            public void inc(float value) {
                this.value += value;
            }
        }

        public class Skills {
            public Skill hacking = new Skill("Hacking");

            public Skills() {

            }
        }


	}
}


