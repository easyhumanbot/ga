﻿using UnityEngine;
using System.Collections;

public class DoorOpen : IKAnimation, IAnimation {
	Quaternion startRot;
	Quaternion endRot;



	public void startAnimation () {
		startRot = obj.transform.localRotation;
		endRot = Quaternion.Euler (0, -88, 0);

	}

	public void update (float time) {
		obj.transform.localRotation = Quaternion.Lerp (startRot, endRot, time / animationTime);
	}

}
