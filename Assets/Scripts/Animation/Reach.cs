﻿using UnityEngine;
using System.Collections;
using RootMotion.FinalIK;



public class Reach : IKAnimation, IAnimation {
	

	protected Vector3 startPos;
	protected Quaternion startRot;


	protected GameObject leftHand { get { return character.leftHandTarget; } }

	public void startAnimation() {
		obj = leftHand;

		obj.transform.parent = to;

		startPos = obj.transform.localPosition;
		startRot = obj.transform.localRotation;
	}

	public void update(float time) {
		obj.transform.localPosition = Vector3.Lerp (startPos, Vector3.zero, time / animationTime);
		obj.transform.localRotation = Quaternion.Lerp (startRot, Quaternion.Euler (Vector3.zero), time / animationTime);
	}


}

