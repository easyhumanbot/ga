﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class AnimationContainer : System.Object {
	public string animationName;

	protected Character character;

	public GameObject obj;
	public Transform from;
	public Transform to;
	public float animationTime = 0.5f;
	public float startTime = 0f;


	public IAnimation an;

	public void Init() {
		if (animationName == "Reach") {
			an = new Reach ();
		}
		if (animationName == "ReachBack") {
			an = new ReachBack ();
		}
		if (animationName == "DoorOpen") {
			an = new DoorOpen ();
		}

		an.SetObj (obj);
		an.SetFrom (from);
		an.SetTo (to);
		an.SetAnimationTime (animationTime);
		an.SetStartTime (startTime);

	}
}