﻿using UnityEngine;
using System.Collections;

public class ReachBack : Reach, IAnimation {

	Vector3 endPos;

	public void startAnimation() {
		obj = leftHand;

		//obj.transform.parent = character.childs ["LeftHandPosition"];

		startPos = obj.transform.position;
		startRot = obj.transform.rotation;

	}

	public void update(float time) {
		obj.transform.position = Vector3.Lerp (startPos, character.childs ["LeftHandPosition"].position, time / animationTime);
		obj.transform.rotation = Quaternion.Lerp (startRot, character.childs ["LeftHandPosition"].rotation, time / animationTime);
	}

	public void Finish() {
		obj.transform.parent = character.childs ["LeftHandPosition"];
	}
}
