﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;


public class Sequence : MonoBehaviour {
	public List<AnimationContainer> animations = new List<AnimationContainer> ();
	public int cur = 0;
	public Character character;

	float time = 0f;
	bool allExecuted;

	public void Start() {
		enabled = false;

		foreach (AnimationContainer container in animations) {
			container.Init ();
		}


	}

	//IAnimation curAnimation { get { return animations [cur].an; } }

	public void Play() {

		time = 0f;
		foreach (AnimationContainer container in animations) {
			container.an.SetCharacter (character);
			container.an.startAnimation ();
		}


		enabled = true;


	}

	public void Update() {
		allExecuted = true;
		time += Time.deltaTime;
		foreach(AnimationContainer container in animations) {
			IAnimation an = container.an;
			if (time >= an.GetStartTime () && time <= an.GetStartTime() + an.GetAnimationTime()) {
				if (!an.ExecutedInPrevUpdate ()) {
					an.startAnimation ();
				}

				allExecuted = false;
				an.update (time - an.GetStartTime ());
				an.SetExecutedInPrevUpdate (true);
			} else if (an.ExecutedInPrevUpdate()) {
				an.Finish ();
				an.SetExecutedInPrevUpdate (false);
			}


		}


		if (allExecuted) {
			enabled = false;
		}

	}
}


public class IKAnimation : IAnimation {
	public Character character;
	public GameObject obj;
	public Transform from;
	public Transform to;

	public float startTime = 0f;
	public float animationTime;


	public bool _executedInPrevUpdate;

	public void startAnimation() {
	}

	public void update(float time) {
	}


	public void SetCharacter(Character ch) {
		character = ch;
	}

	public void SetFrom(Transform from) {
		this.from = from;
	}

	public void SetTo(Transform to) {
		this.to = to;
	}

	public void SetAnimationTime(float time) {
		animationTime = time;
	}

	public void SetStartTime(float time) {
		startTime = time;
	}

	public void SetObj(GameObject obj) {
		this.obj = obj;
	}

	public float GetStartTime() {
		return startTime;
	}

	public float GetAnimationTime() {
		return animationTime;
	}

	public bool ExecutedInPrevUpdate() {
		return _executedInPrevUpdate;
	}

	public void SetExecutedInPrevUpdate(bool value) {
		_executedInPrevUpdate = value;
	}

	public void Finish() {
	
	}
}

public interface IAnimation {

	void startAnimation ();

	void update (float time);

	void SetCharacter (Character ch);

	void SetFrom(Transform from);

	void SetTo(Transform to);

	void SetAnimationTime(float time);

	void SetStartTime(float time);

	float GetAnimationTime ();

	void SetObj(GameObject obj);

	float GetStartTime ();

	bool ExecutedInPrevUpdate();

	void SetExecutedInPrevUpdate (bool value);

	void Finish();
}