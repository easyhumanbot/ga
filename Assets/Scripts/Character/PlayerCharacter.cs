﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UMA;

public class PlayerCharacter : Character, IMovable {

    List<Character> visibleCharacters = new List<Character>();

    public void Create() {
        base.Create();

        umaData.OnCharacterCreated += OnPlayerCharacterCreated;
        defaultAimTime = 0.5f;
    }

    private void OnPlayerCharacterCreated(UMAData umaData) {
        SetVisible(true);
    }

    void Update() {

        base.Update();

        
        IList<RAIN.Entities.Aspects.RAINAspect> aspects = aiRig.AI.Senses.Sense("Visual", "NPC", RAIN.Perception.Sensors.RAINSensor.MatchType.BEST);
        foreach (RAIN.Entities.Aspects.RAINAspect aspect in aspects) {
            Character character = aspect.Entity.Form.GetComponent<Character>();
            character.SetVisible(true);
            character.lastVisible = Time.time;

            if (!visibleCharacters.Contains(character)) {
                visibleCharacters.Add(character);
                //GameManager.Instance.Pause();
            }
        }

        for (int i = visibleCharacters.Count - 1; i >= 0; i--) {
            Character character = visibleCharacters[i];

            if (character.isDead) {
                visibleCharacters.RemoveAt(i);
            } else if (Time.time - character.lastVisible > 1f) {
                //character.SetVisible(false);
                visibleCharacters.RemoveAt(i);
            }
        }

    }
}