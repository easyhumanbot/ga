﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Pathfinding;
using UMA;

using RootMotion.FinalIK;
using RAIN.Core;

public delegate void OnDestinationReached();

public static class CharacterUtil {
	public static List<string> getLimbNames() {
		List<string> limbs = new List<string>();
		limbs.Add ("Hips");
		limbs.Add ("Spine");
		limbs.Add ("RightUpLeg");
		limbs.Add ("RightLeg");
		limbs.Add ("LeftUpLeg");
		limbs.Add ("LeftLeg");
		limbs.Add ("RightArm");
		limbs.Add ("RightForeArm");
		limbs.Add ("LeftArm");
		limbs.Add ("LeftForeArm");
		return limbs;
	}
}

public class PathFind {
}

public interface IMovable {
	void startPath(Vector3 position, bool slide = false, OnPathDelegate otherDelegate = null, OnDestinationReached destinationReached = null);
	void stop ();
}


public class Character : UMAMaker1, IMovable {
    public Game.Character gameCharacter;
    public bool controlledByPlayer = false;

	public bool isDead = false;
	public float newPose = 0f;
    public float lastVisible = -1f;

    public AIRig aiRig;
    public RAIN.Entities.EntityRig entityRig;

    public Dictionary<string, Transform> childs = new Dictionary<string, Transform>();

	private GameObject nullAim;
	private GameObject aim;
	public GameObject target;
	public float rotationSpeed = 9f;
	public bool isRotating = false;

    public Animator animator;
    private CharacterController cc;
    private Rigidbody rb;
    private IKSolverFullBodyBiped ik;
    
    public ItemInfo weapon;
	public GameObject leftHandTarget;
	bool isInteracting = false;
	public IKSolverFullBodyBiped iksolver;


    void Awake() {
        

        cc = GetComponent<CharacterController>();
        rb = GetComponent<Rigidbody>();
        aiRig = GetComponent<AIRig>();
        entityRig = GetComponent<RAIN.Entities.EntityRig>();
        seeker = GetComponent<Seeker>();
    }

	protected void Start () {
        
    }

    public void Create(Game.Character gameCharacter = null) {
        if (gameCharacter != null) {
            this.gameCharacter = gameCharacter;
        } else {
            this.gameCharacter = new Game.Character();
        }

        target = new GameObject("_target");
        nullAim = new GameObject("_nullAim");
        aim = new GameObject("Aim Target");
        aim.transform.position = transform.position + transform.forward * 10f + new Vector3(0, 1.5f, 0);


        slotLibraray = (SlotLibrary)UMAContext.FindInstance().slotLibrary;
        overlayLibrary = (OverlayLibrary)UMAContext.FindInstance().overlayLibrary;
        raceLibrary = (RaceLibrary)UMAContext.FindInstance().raceLibrary;

        
        GenerateRandomUMA(gameCharacter.sex);
        umaData.OnCharacterCreated += OnCharacterCreatedCallback;
        SetSlot(6, "Shoes");
        AddOverlay(6, "Shoes");

        //SetSlot(6, "CowboyHat_Slot");
        //AddOverlay(6, "Camohat");
    }

    public delegate void CharacterLoadedCallback(Character character);
    public CharacterLoadedCallback characterLoadedCallback;

    private void OnCharacterCreatedCallback(UMAData umaData)
	{
		getChilds ();

		customizeCharacter ();

		GetComponent<FullBodyBipedIK> ().GetIKSolver ().OnPostInitiate += onPostInitiate;
		StartCoroutine (initFinalIK ());
		ik = GetComponent<FullBodyBipedIK> ().solver;

        //childs ["Hips"].transform.parent = childs ["Position"];
        configureRagdoll();

        InitAiming();

        

        SetVisible(false);
        //aiRig.AI.WorkingMemory.SetItem("combatMode", true);

        if (characterLoadedCallback != null) {
            characterLoadedCallback(this);
        }
    }

    

	public float sinceStart = 0f;

	IEnumerator initFinalIK() {
		while (sinceStart < 1f) {
			sinceStart += 0.1f;
			yield return new WaitForSeconds(0.1f);
		}
		GetComponent<FullBodyBipedIK> ().AutoDetectReferences ();
		yield break;
	}

	void onPostInitiate() {
		configureInteractionSystem ();
	}

	void configureInteractionSystem() {
		InteractionSystem isys = GetComponent<InteractionSystem> ();
		//isys.collider = childs ["Hips"].gameObject.GetComponent<Collider> ();
		isys.enabled = true;
	}

	void customizeCharacter() {
        GameObject root = transform.FindChild("Root").gameObject;
        root.transform.localScale = new Vector3 (1.2f, 1.2f, 1.2f);
		root.GetComponent<Renderer> ().materials [0].SetFloat ("_BumpScale", 0.6f);

		Shader shader;
		shader = Shader.Find("Toon/Lit Outline");
		foreach (Material material in root.GetComponent<Renderer> ().materials) {
			material.shader = shader;
			material.SetFloat ("_Outline", 1f);
		}
	}

	void configureRagdoll() {
		RagdollFactoryGenerated factory = new RagdollFactoryGenerated ();
		factory.CreateRagdollComponents (transform.FindChild("Root").FindChild("Global").gameObject);
		foreach (Rigidbody rb in GetComponentsInChildren<Rigidbody> ()) {
			rb.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
		}
	}

    
	void InitAiming() {
		IKSolverAim aimIK = GetComponent<RootMotion.FinalIK.AimIK> ().solver;
        
		aimIK.AddBone (childs["Hips"]);
		aimIK.AddBone (childs["LowerBack"]);
		aimIK.AddBone (childs["Spine"]);
		aimIK.AddBone (childs["Spine1"]);
		aimIK.AddBone (childs["Neck"]);

		aiRig.AI.Senses.GetSensor ("Visual").MountPoint = childs ["Head"];
        

        /*
		IKSolverLookAt solver = GetComponent<RootMotion.FinalIK.LookAtIK> ().solver;
		solver.head = new IKSolverLookAt.LookAtBone(childs ["Head"]);
		solver.target = aim.transform;
		*/
        //leftHandTarget = new GameObject("leftHandTarget");
        

		
	}



    void getChilds() {
        childs.Clear();
        getChilds(transform);
    }

	void getChilds(Transform transform) {
		foreach(Transform t in transform)
		{
            t.gameObject.layer = gameObject.layer;
            if (!childs.ContainsKey(t.name)) {
                childs.Add(t.name, t);
            } else {
                Debug.Log("Contains already " + t.name);
            }
			getChilds (t);
		}
	}

	void OnMouseDown(){
		
	}   


	public void die() {
		if (isDead) {
			return;
		}
		isDead = true;

        
        if (weapon != null) Unequip();

		foreach (Rigidbody rb in GetComponentsInChildren<Rigidbody> ()) {
			rb.isKinematic = false;
		}

		GetComponent<CharacterController> ().enabled = false;
		GetComponent<AimIK> ().enabled = false;
		//GetComponent<SimpleAimingSystem> ().enabled = false;
		GetComponent<FullBodyBipedIK> ().enabled = false;
		GetComponent<Animator> ().enabled = false;
		GetComponent<Character> ().enabled = false;
        GetComponent<AIRig>().enabled = false;


        Destroy(GetComponent<RAIN.Entities.EntityRig>());
	}


	protected void Update () {
		base.Update ();
		if (childs.Count == 0 || animator == null) {
			return;
		}

        if (controlledByPlayer) {
            Detect();
        }

		proccessPose ();
		proccessMove ();
        
        if (IsWeaponEquipped()) {
            proccessIK();
            proccessFire();
        }
    }

	bool _interacting = false;
    
 
	protected void LateUpdate() {
        if (childs.Count == 0 || animator == null || !IsWeaponEquipped()) {
            return;
        }
        if (animator.GetFloat("Aim") == 0) {
            return;
        }

        if (leftHandTarget != null && ik.initiated) {
			ik.leftHandEffector.target = leftHandTarget.transform;
			ik.leftHandEffector.positionWeight = 1f;
			ik.leftHandEffector.rotationWeight = 1f;
		}
        
		GetComponent<AimIK> ().solver.Update ();
	}
    

	protected void proccessPose() {
		float pose = animator.GetFloat ("Pose");
		float delta = 0.1f;
		float error = delta / 2f;
		if (pose < newPose - error) {
			animator.SetFloat ("Pose", pose + delta);
		} else if (pose > newPose + error) {
			animator.SetFloat ("Pose", pose - delta);
		}
	}

	protected void proccessIK(bool instant = false) {
        if (animator.GetFloat("Aim") == 0) {
            return;
        }

		if (!instant && Vector3.Distance (aim.transform.position, target.transform.position) > 0.3f) {
			isRotating = true;
		} else {
			isRotating = false;
		}

		if (isRotating) {
			nullAim.transform.position = childs ["Head"].position;
			var _direction = (target.transform.position - nullAim.transform.position).normalized;
			var _lookRotation = Quaternion.LookRotation (_direction);
			nullAim.transform.rotation = Quaternion.Slerp (nullAim.transform.rotation, _lookRotation, Time.deltaTime * rotationSpeed);
			aim.transform.position = nullAim.transform.position + nullAim.transform.forward * Vector3.Distance (target.transform.position, nullAim.transform.position);
		} 
		if (!isRotating) {
			aim.transform.position = target.transform.position;
		}
		if (slideMove == false && animator.GetFloat ("Speed") > 0) {
			target.transform.position = transform.position + transform.forward * 10f + new Vector3 (0, 1.9f, 0);
		} else if (slideMove && animator.GetFloat("Speed") > 0) {
            if (slideDirection.x > 0.0) {
                target.transform.position = transform.position + Vector3.Cross(transform.forward.normalized + new Vector3(0, 0, 0), new Vector3(0, 1, 0)) * 10f + new Vector3(0, 1.9f, 0);
            } else if (slideDirection.x < 0.0) {
                target.transform.position = transform.position + Vector3.Cross(transform.forward.normalized + new Vector3(0, 0, 0), new Vector3(0, -1, 0)) * 10f + new Vector3(0, 1.9f, 0);
            }
            
            /*
            if (previousPosition != null) {
                target.transform.position = target.transform.position - (previousPosition - transform.position);
            }
            */
        }
	}

	void FixedUpdate() {
		if (animator == null) {
			if (umaData != null && umaData.animator != null) {
				animator = umaData.animator;
			}
		}
	}
		


	/* IMovable */
	Seeker seeker;
	Path path;
	public float speed = 2.0f;
	float nextWaypointDistance = 0.5f;
	int currentWaypoint = 0;
	OnDestinationReached destinationReached;
	bool slideMove = false;
    Vector3 slideDirection;
    Vector3 previousPosition;

	public void startPath(Vector3 position, bool slide = false, OnPathDelegate otherDelegate = null, OnDestinationReached destinationReached = null) {
		slideMove = slide;
        if (slideMove) {
            slideDirection = weapon.prefab.GetComponentInChildren<Weapon>().transform.InverseTransformPoint(position);
        }

        this.destinationReached = destinationReached;
		otherDelegate += OnPathComplete;
		seeker.StartPath (transform.position, position, otherDelegate);
        aiRig.AI.WorkingMemory.SetItem("isMoving", true);
    }

	public void stop() {
        aiRig.AI.WorkingMemory.SetItem("isMoving", false);
		animator.SetFloat ("Speed", 0f);
		path = null;
	}

	void proccessMove() {
		if (animator == null) {
			return;
		}
		if (path != null) {
			if (currentWaypoint >= path.vectorPath.Count) {
				stop ();

				if (destinationReached != null) {
					destinationReached ();
				}
				return;
			}
            if (transform.position != previousPosition) {
                previousPosition = transform.position;
            }
			Vector3 dir = (path.vectorPath[currentWaypoint]-transform.position).normalized;
			dir *= speed;

            
			cc.Move((dir + Physics.gravity) * Time.deltaTime);            
            //rb.AddForce((dir) * Time.deltaTime * 4, ForceMode.VelocityChange);

            var newRotation =  Quaternion.LookRotation(dir).eulerAngles;
			newRotation.z = newRotation.x = 0;
			transform.rotation = Quaternion.Euler (newRotation); 

			animator.SetFloat ("Speed", 1f);

			if (Vector3.Distance (transform.position,path.vectorPath[currentWaypoint]) < nextWaypointDistance) {
				currentWaypoint++;
			}
		}
	}

	public void OnPathComplete (Path p) {
		if (!p.error) {
            path = p;
			currentWaypoint = 0;
		}
	}




	public void toggleWeapon() {
		if (animator.GetBool("Arm") == true) { 
			animator.SetBool ("Arm", false);
		} else {
			animator.SetBool ("Arm", true);
		}
	}


	public void changePose(float pose) {
		newPose = pose;
	}


	public void stand() {
		changePose (0f);
	}

	public void crouch() {
		changePose (1f);
	}

	public void interact() {
		isInteracting = true;


		if (lastCollider == null) {
			return;
		}

		Sequence seq = lastCollider.GetComponent<Sequence> ();	
		seq.character = this;
		seq.Play ();
	}


	private bool isAiming = false;
	private float aimTimeLast = 0f;
	public int shotsToMake = 0;
	public float handCursor = 0f;
	private float coilTimeDelay = 0f;
	private Vector3 placeToShoot;
	private GameObject targetToShoot;
    public float defaultAimTime = 1.5f;
    public bool targetIsVisible = false;
    public float lastSeen = 0f;
    public void shoot(Vector3 t) {
		if (isAiming) {
			return;
		}
		stop ();
        targetIsVisible = true;

		target.transform.position = t;
		placeToShoot = target.transform.position;
		targetToShoot = null;


        aimTimeLast = defaultAimTime;
		shotsToMake = 30;
	}

	public void shootCharacter(GameObject t) {
		if (isAiming) {
			return;
		}

        Character character = t.GetComponent<Character>();
        if (character.isDead) {
            return;
        }

        GameObject spine = character.GetSpine();
        if (spine == null) {
            return;
        }

        stop ();
        targetIsVisible = true;
        

		target.transform.position = spine.GetComponent<Collider> ().bounds.center;
		target.transform.parent = spine.transform;
		placeToShoot = Vector3.zero;
		targetToShoot = t;


        aimTimeLast = defaultAimTime;
		shotsToMake = 30;
	}

    public GameObject GetSpine() {
        if (childs.ContainsKey("Spine")) {
            return childs["Spine"].gameObject;
        }
        return null;
    }

	private void proccessFire() {
        if (isRotating && handCursor <= 0) {
            lastSeen = Time.time;
            return;
        }
		if (shotsToMake <= 0 && handCursor <= 0 && !isAiming) {
			return;
		}

        if (targetToShoot != null) {
            IList<RAIN.Entities.Aspects.RAINAspect> aspects = aiRig.AI.Senses.SenseAll();
            //IList<RAIN.Entities.Aspects.RAINAspect> aspects = aiRig.AI.Senses.Sense("Visual", "Player", RAIN.Perception.Sensors.RAINSensor.MatchType.BEST);
            foreach (RAIN.Entities.Aspects.RAINAspect aspect in aspects) {
                if (aspect.AspectType == "visual" && aspect.Entity.Form == targetToShoot) {
                    lastSeen = Time.time;
                }
            }

            if (Time.time - lastSeen > 0.3f) {
                targetIsVisible = false;
                shotsToMake = 0;
                isAiming = false;
                handCursor = 0;
                target.transform.parent = null;

                return;
            }
            

            /*
            if (aiRig.AI.Senses.IsDetected("Visual", targetToShoot, "Player") == null && Time.time - lastSeen > 0.6f) {
                targetIsVisible = false;
                shotsToMake = 0;
                isAiming = false;
                handCursor = 0;
                
                return;
            } else {
                lastSeen = Time.time;
            }
            */
        }


		if (shotsToMake > 0) {
			isAiming = true;
			if (aimTimeLast > 0) {
				aimTimeLast -= Time.deltaTime;
				return;
			}

			handCursor = weapon.prefab.GetComponentInChildren<Weapon>().fireTime * shotsToMake;
			shotsToMake = 0;
			return;
		}

		if (handCursor > 0) {
			if (weapon.prefab.GetComponentInChildren<Weapon>().wait <= 0) {
                float d = Vector3.Distance(target.transform.position, gameObject.transform.position) / 50 * 1.5f;

				float x = UnityEngine.Random.Range (-1f, 1f) * d;
				float y = UnityEngine.Random.Range (-1f, 1f) * d;
				float z = UnityEngine.Random.Range (-1f, 1f) * d;
				aim.transform.position += new Vector3 (x, y, z);

				float missChance = 0.0f;
				if (UnityEngine.Random.value <= missChance) {
                    target.transform.position += new Vector3 (0.7f, 0.7f, 0.7f);
				}

                GetComponent<AimIK>().solver.Update();

                weapon.prefab.GetComponentInChildren<Weapon>().fire ();
				coilTimeDelay = weapon.prefab.GetComponentInChildren<Weapon>().wait / 2f;
			}

			if (coilTimeDelay <= 0) {
				if (targetToShoot != null) {
					Character character = targetToShoot.GetComponent<Character> ();
					GameObject spine = character.childs ["Spine"].gameObject;
					target.transform.position = spine.GetComponent<Collider> ().bounds.center;
					target.transform.parent = spine.transform;
				} else if (placeToShoot != Vector3.zero) {
					target.transform.position = placeToShoot;
				}
			}

			handCursor -= Time.deltaTime;
			coilTimeDelay -= Time.deltaTime;

			return;
		}



		isAiming = false;
	}



	private Collider lastCollider;

	void OnTriggerEnter(Collider other) {
		lastCollider = other;
	}

	void OnTriggerExit(Collider other) {
		lastCollider = null;
	}



    //Artificial Intelligence
    List<Character> visibleCharacters = new List<Character>();
    private void Detect() {
        IList<RAIN.Entities.Aspects.RAINAspect> aspects = aiRig.AI.Senses.Sense("Visual", "NPC", RAIN.Perception.Sensors.RAINSensor.MatchType.BEST);
        foreach (RAIN.Entities.Aspects.RAINAspect aspect in aspects) {
            Character character = aspect.Entity.Form.GetComponent<Character>();
            if (character != null) {
                character.SetVisible(true);
                character.lastVisible = Time.time;
            }

            if (!visibleCharacters.Contains(character)) {
                visibleCharacters.Add(character);
                //GameManager.Instance.Pause();
            }
        }

        for (int i = visibleCharacters.Count - 1; i >= 0; i--) {
            Character character = visibleCharacters[i];

            if (character.isDead) {
                visibleCharacters.RemoveAt(i);
            } else if (Time.time - character.lastVisible > 1f) {
                //character.SetVisible(false);
                visibleCharacters.RemoveAt(i);
            }
        }
    }


    public void ControlByPlayer() {
        controlledByPlayer = true;
        aiRig.enabled = false;
    }

    public void ControlByAI() {
        controlledByPlayer = false;
        aiRig.enabled = true;
    }

    public void SetVisible(bool visible) {
        GetComponentInChildren<SkinnedMeshRenderer>().enabled = visible;
        foreach(MeshRenderer renderer in GetComponentsInChildren<MeshRenderer>()) {
            renderer.enabled = visible;
        }
    }


    public void SetGuardMode(LevelGenerator.Room room) {
        aiRig.AI.WorkingMemory.SetItem("combatMode", false);
        aiRig.AI.WorkingMemory.SetItem("guardMode", true);

        LevelGenerator.Util.addCubeGizmo(room.doors[0].transform.position, new Color(1, 1, 1, 1));
        Vector3 dir = room.doors[0].transform.position - transform.position;
        startPath(transform.position + dir.normalized);        
    }

    public void SetCombatMode() {
        aiRig.AI.WorkingMemory.SetItem("combatMode", true);
    }

    //Inventory
    public void EquipWeaponFromInventory() {
        foreach(ItemInfo item in gameCharacter.inventory) {
            Equip(item);
            break;
        }
    }

    public bool IsWeaponEquipped() {
        return !ReferenceEquals(weapon, null);
    }

    public void Equip(ItemInfo weap) {
        weap.Create();
        weapon = weap;

        

        if (weapon.prefab.GetComponent<Rigidbody>()) Destroy(weapon.prefab.GetComponent<Rigidbody>());
        if (weapon.prefab.GetComponentInChildren<MeshCollider>()) weapon.prefab.GetComponentInChildren<MeshCollider>().enabled = false;

        weapon.prefab.transform.parent = childs["RightHand"];
        weapon.prefab.transform.localPosition = new Vector3(0, 0, 0);
        weapon.prefab.transform.localEulerAngles = new Vector3(0, 0, 0);

        getChilds();

        weapon.prefab.transform.localPosition = childs["_position"].transform.localPosition;
        weapon.prefab.transform.localRotation = childs["_position"].transform.localRotation;

        GetComponent<AimIK>().solver.transform = childs["Gun"];
        GetComponent<AimIK>().solver.target = aim.transform;

        leftHandTarget = new GameObject("LeftHandTarget");
        leftHandTarget.transform.parent = childs["LeftHandPosition"];
        leftHandTarget.transform.localPosition = Vector3.zero;
        leftHandTarget.transform.localRotation = Quaternion.Euler(Vector3.zero);

        animator.SetFloat("Aim", 1);
    }

    public void Unequip() {
        if (leftHandTarget != null) Destroy(leftHandTarget);
        weapon.prefab.AddComponent<Rigidbody>();
        weapon.prefab.GetComponentInChildren<MeshCollider>().enabled = true;
        weapon.prefab.transform.parent = null;
        weapon = null;

        animator.SetFloat("Aim", 0);
    }
    

    public void reload() {
        weapon.prefab.GetComponentInChildren<Weapon>().clipSize = 30;
    }

    public void Say(string text) {
        GetComponent<HoverText>().Say(text);
    }
}

