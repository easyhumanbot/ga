﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HoverText : MonoBehaviour {

    public GameObject canvas;
    public Text text;

    string textToSay = "Drop your gun!";
    int i = 0;
    float lastTime = 0;

	void Start () {
        canvas.transform.parent = null;

        text.text = "";
	}
	
	void Update () {
        canvas.transform.position = transform.position + new Vector3(0.4f, 1.7f, 0.45f);

        if (Time.time - lastTime > 0.050f) {
            ShowNextLetter();
            lastTime = Time.time;
        }
    }

    void ShowNextLetter() {
        if (i <= textToSay.Length) {
            text.text = textToSay.Substring(0, i);
        } else if ((i - textToSay.Length) * 0.050f >= 1.200f) {
            canvas.SetActive(false);
            enabled = false;
        }
        i++;
    }

    public void Say(string text) {
        i = 0;
        textToSay = text;
        this.text.text = "";
        enabled = true;
        canvas.SetActive(true);
    }
}
