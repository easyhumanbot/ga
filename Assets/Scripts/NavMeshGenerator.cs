﻿using UnityEditor;
using UnityEngine;
using System.Collections.Generic;
class NavmeshGenerator : MonoBehaviour
{
	[MenuItem("Terrain/Create Navmesh from Terrain")]
	static void RayGenerateNavMesh()
	{
		int SamplesPerLine = 10;
		int LineSamples = 10;
		int RayCasterHeight = 1000;

		float X_PerSample = (Terrain.activeTerrain.terrainData.size.x) / (SamplesPerLine - 1);
		float Z_PerSample = (Terrain.activeTerrain.terrainData.size.z) / (LineSamples - 1);
		float terrainX = Terrain.activeTerrain.transform.position.x;
		float terrainZ = Terrain.activeTerrain.transform.position.z;
		Vector3 currPos = new Vector3(terrainX, RayCasterHeight, terrainZ);
		RaycastHit hitInfo;

		//Iteration Vars
		List<Vector3> vertexes = new List<Vector3>();

		for (int x = 0; x < LineSamples; x++)
		{
			for (int z = 0; z < SamplesPerLine; z++)
			{
				Physics.Raycast(currPos, Vector3.down, out hitInfo, Mathf.Infinity);
				//If Normal vs raycast angle is too great (which is what?) then do not add point
				vertexes.Add(hitInfo.point);
				currPos.z += Z_PerSample;
			}
			currPos.x += X_PerSample;
			currPos.z = 0;
		}

		//Generate the mesh and set it's verticies to raycasted points
		Mesh navMesh = new Mesh();
		navMesh.name = "NavagationMesh";
		navMesh.vertices = vertexes.ToArray();

		//Link Triangles from vertexes
		List<int> tris = new List<int>();
		int vert;
		for (int line = 0; line < LineSamples - 1; line++)
		{
			for (int i = 0; i < SamplesPerLine - 1; i++)
			{
				vert = (SamplesPerLine * line) + i; //current vertex in a linear list
				//generate quad
				//tri 1
				tris.Add(vert);
				tris.Add(vert + 1);
				tris.Add(vert + SamplesPerLine);
				//tri 2
				tris.Add(vert + 1);
				tris.Add(vert + SamplesPerLine);
				tris.Add(vert + SamplesPerLine + 1);
			}
		}

		navMesh.triangles = tris.ToArray();

		GameObject NavObject = new GameObject();
		NavObject.name = "NavMesh";
		NavObject.AddComponent<MeshRenderer>();
		NavObject.AddComponent<MeshFilter>();
		MeshFilter Filter = (MeshFilter)NavObject.GetComponent("MeshFilter");
		Filter.mesh = navMesh;

	}

	// Validate the menu item.
	// The item will be disabled if there is no active terrain
	[MenuItem("Terrain/Create Navmesh from Terrain", true)]
	static bool ValidateRayCastMeshConstruction()
	{
		//return (Selection.activeObject is Terrain);
		return Terrain.activeTerrain != null;
	}
}