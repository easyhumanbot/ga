﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using LevelGenerator;


namespace LSystemAlgorithm {

	public class LSystem {
		public string seq;



		public LSystem() {
			seq = String.Format ("{0}+{1}+{0}+{1}", _s ("F", 5), _s ("F", 7));

			//seq = seq.Replace ("F+F", ")D)");

		}

		public string iterate(int n = 1) {
			
			for (int i = 0; i < n; i++) {
				int _c = 0;
				while (true) {
					string prevseq = seq;
					_iterate ();

					if (seq.Contains ("--")) {
						seq = prevseq;
					} else {
						break;
					}

					_c++;

					if (_c > 10) {
						throw new Exception ("Too deep cycle.");
					}
				}
			}
			

			return seq;
		}

		public static bool isLSeqValid(string seq) {
			GameObject turtleObject = new GameObject ("Turtle");
			Turtle turtle = turtleObject.AddComponent<Turtle>() as Turtle;

			turtle.seq (seq, true);

			turtle.destroy ();
			return (turtle.intersections == 0);
		}


		public static Area createAreaFromLSeq(string seq, bool draw = false) {
			GameObject turtleObject = new GameObject ("Turtle");
			Turtle turtle = turtleObject.AddComponent<Turtle>() as Turtle;

			turtle.seq (seq, draw);

			List<Vector3> vertices = turtle.vertices;

			turtle.destroy ();
			return new Area (vertices);
		}

		string _s(string c, int length) {
			string s = "";
			for (int i = 0; i < length; i++) {
				s += c;
			}
			return s;
		}


		void _iterate() {
			string expr = @"\bF+\b";
			MatchCollection mc = Regex.Matches(seq, expr);


			int rand = UnityEngine.Random.Range (0, mc.Count);
			Match m = mc [rand];

			if (m.Length >= 5) {
				int bLength = UnityEngine.Random.Range (0, 3);

				int pLength = UnityEngine.Random.Range (5, m.Length - bLength);
				int pHeight = UnityEngine.Random.Range (6, m.Length);

				float ratio = (float)pLength / (float)pHeight;

				seq = seq.Remove (m.Index, m.Length).Insert (m.Index, String.Format ("{3}-{2}+{0}+{2}-{1}", 
					_s ("F", pLength), 
					_s ("F", m.Length - pLength - bLength),
					_s ("F", pHeight),
					_s ("F", bLength)
				));
			}
		}




		public void draw() {
			GameObject turtleObject = new GameObject ("Turtle");
			Turtle turtle = turtleObject.AddComponent<Turtle>() as Turtle;

			turtle.seq (seq);

		}


	}




	public class Turtle : MonoBehaviour {
		
		bool draw = false;
		public int intersections = 0;

		public List<Vector3> vertices = new List<Vector3>();
		public List<Vector3> allVertices = new List<Vector3>();

		public Turtle() {
			transform.position = new Vector3 (1, 0, 1);
			transform.rotation = Quaternion.Euler (0, 0, 0);

			_addVertice ();
		}

		public void seq(string seq, bool draw = false) {
			this.draw = draw;

			for (int i = 0; i < seq.Length; i++) {

				if (seq[i] == 'F') {
					forward (1);
				}
				if (seq[i] == 'D') {
					forward ((float)Math.Sqrt(2));
				}
				if (seq[i] == '+') {
					rotate (90);
				}
				if (seq[i] == '-') {
					rotate (-90);
				}
				if (seq[i] == ')') {
					rotate (45);
				}
			}
		}

		public void destroy() {
			Destroy (this.gameObject);
		}

		public void _addVertice() {
			vertices.Add (transform.position);
		}

		public void forward(float length) {
			if (draw) {
				Util.addLine (transform.position, transform.position + transform.forward * length);
			}

			allVertices.Add (transform.position);

			transform.position = transform.position + transform.forward * length;

			if (allVertices.Contains (transform.position)) {
				intersections++;

				Debug.Log (intersections);
			}


			_addVertice ();
		}

		public void drawAreas() {
		}

		public void drawRectangle() { 
			
		}


		public void rotate(int angle) {
			transform.Rotate (0, angle, 0);

			allVertices.Add (transform.position);
			_addVertice ();
		}


		public void Update() {

			//Debug.Log(isPointInsideArea (vertices, transform.position));
		}



	}
}
