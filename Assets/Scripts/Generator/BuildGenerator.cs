﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LSystemAlgorithm;
using LevelGenerator;
using UMA;
using Random = UnityEngine.Random;

namespace LevelGenerator {
    public class RoomConnection {
        public Room room;
        public Point connection;
        public RoomConnection(Room room, Point connection) {
            this.room = room;
            this.connection = connection;
        }
    }

    public class Point {
        public int x;
        public int y;
        public List<RoomConnection> roomConnections = new List<RoomConnection>();
        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }



    public class FillAlgorithm {
        protected class ListPoint : List<Point> {
            public ListPoint(IEnumerable<Point> points) : base(points) { }
            public ListPoint() : base() { }

            public ListPoint GetEdgePointsBottom() {
                ListPoint list = new ListPoint();
                foreach (Point p in this) {
                    if (p.y == 0) {
                        list.Add(p);
                    }
                }
                return list;
            }
            
            public ListPoint ConnectsWithAnyRoom(List<List<int>> area, Size s, List<Room> rooms) {
                return ConnectsWithAnyRoom(area, s, new ListRoom(rooms));
            }

            public ListPoint ConnectsWithAnyRoom(List<List<int>> area, Size s, ListRoom rooms) {
                ListPoint list = new ListPoint();
                foreach (Point p in this) {
                    bool isValid = false;
                    for (int x = (int)p.x; x < p.x + s.width; x++) {
                        for (int y = (int)p.y; y < p.y + s.height; y++) {
                            if (x - 1 > 0 && area[x - 1][y] != 0) {
                                Room room = rooms.getRoomByNumber(area[x - 1][y]);
                                if (room != null && room.noDoors == false) {
                                    p.roomConnections.Add(new RoomConnection(room, new Point(x, y)));
                                    isValid = true;
                                }
                            } else if (x + 1 < area.Count && area[x + 1][y] != 0) {
                                Room room = rooms.getRoomByNumber(area[x + 1][y]);
                                if (room != null && room.noDoors == false) {
                                    p.roomConnections.Add(new RoomConnection(room, new Point(x, y)));
                                    isValid = true;
                                }
                            } else if (y - 1 > 0 && area[x][y - 1] != 0) {
                                Room room = rooms.getRoomByNumber(area[x][y - 1]);
                                if (room != null && room.noDoors == false) {
                                    p.roomConnections.Add(new RoomConnection(room, new Point(x, y)));
                                    isValid = true;
                                }
                            } else if (y + 1 < area[0].Count && area[x][y + 1] != 0) {
                                Room room = rooms.getRoomByNumber(area[x][y + 1]);
                                if (room != null && room.noDoors == false) {
                                    p.roomConnections.Add(new RoomConnection(room, new Point(x, y)));
                                    isValid = true;
                                }
                            }
                        }
                    }

                    if (isValid) {                        
                        list.Add(p);
                    }

                }
                return list;
            }

            public ListPoint ConnectsWithRoom(List<List<int>> area, Size s, Room room) {
                
                ListPoint list = new ListPoint();
                foreach (Point p in this) {
                    bool isValid = false;
                    for (int x = (int)p.x; x < p.x + s.width; x++) {
                        for (int y = (int)p.y; y < p.y + s.height; y++) {
                            if (x - 1 > 0 && area[x - 1][y] == room.number) {
                                p.roomConnections.Add(new RoomConnection(room, new Point(x, y)));
                                isValid = true;
                            } else if (x + 1 < area.Count && area[x + 1][y] == room.number) {
                                p.roomConnections.Add(new RoomConnection(room, new Point(x, y)));
                                isValid = true;
                            } else if (y - 1 > 0 && area[x][y - 1] == room.number) {
                                p.roomConnections.Add(new RoomConnection(room, new Point(x, y)));
                                isValid = true;
                            } else if (y + 1 < area[0].Count && area[x][y + 1] == room.number) {
                                p.roomConnections.Add(new RoomConnection(room, new Point(x, y)));
                                isValid = true;
                            }
                        }
                    }

                    if (isValid) {
                        list.Add(p);
                    }
                }
                return list;
            }

            public ListPoint xEquals(int x) {
                ListPoint list = new ListPoint();
                foreach (Point p in this) {
                    if (p.x == x) {
                        list.Add(p);
                    }
                }
                return list;
            }
        }

        protected class Size {
            public int width;
            public int height;
            public Size(int width, int height) {
                this.width = width;
                this.height = height;
            }

            public Size flip() {
                return new Size(height, width);
            }
        }

        protected List<List<int>> area = new List<List<int>>();
        protected List<Room> rooms = new List<Room>();
        int areaWidth;
        int areaHeight;

        public FillAlgorithm(int width, int height){
            areaWidth = width;
            areaHeight = height;

            CreateArea();
        }

        private void CreateArea() {
            area = new List<List<int>>();
            for (int i = 0; i < areaWidth; i++) {
                area.Add(new List<int>());
                for (int j = 0; j < areaHeight; j++) {
                    area[i].Add(0);
                }
            }
        }

        

        protected Room placeRoom(Size s, ListPoint points) {
            return placeRoom(s.width, s.height, points);
        }

        protected Room placeRoom(int roomWidth, int roomHeight, ListPoint points) {
            if (points.Count > 0) {
                Point p = points[Random.Range(0, points.Count)];
                Room room = new Room(p.x, p.y, roomWidth, roomHeight);

                if (p.roomConnections.Count > 0) {
                    room._doorPositions = p.roomConnections;
                }

                rooms.Add(room);

                return room;
            }

            return null;
        }

        protected void vaultRoom() {
            int roomWidth = 6;
            int roomHeight = 4;
            List<Point> points = GetValidPoints(roomWidth, roomHeight);
            if (points.Count > 0) {
                Point p = points[Random.Range(0, points.Count)];
                Room room = new Room(p.x, p.y, roomWidth, roomHeight);
                rooms.Add(room);
            }
        }


        protected void place3x3Rooms() {
            int _count = 0;
            while (true) {
                _count++;
                if (_count > 100) throw new Exception("Too deep cycle.");

                Size s = new Size(3, 3);
                List<Point> points = GetValidPoints(s).ConnectsWithAnyRoom(area, s, new ListRoom(rooms));
                if (points.Count > 0) {
                    Point p = points[Random.Range(0, points.Count)];
                    Room room = new Room(p.x, p.y, s.width, s.height);
                    if (p.roomConnections.Count > 0) {
                        room._doorPositions = p.roomConnections;
                    }
                    //room.roomType = RoomType.HALL;
                    rooms.Add(room);
                } else {
                    break;
                }
            }
        }

        protected void place2x2Rooms() {
            int _count = 0;
            while (true) {
                _count++;
                if (_count > 100) throw new Exception("Too deep cycle.");

                Size s = new Size(2, 2);
                List<Point> points = GetValidPoints(s);
                if (points.Count > 0) {
                    Point p = points[Random.Range(0, points.Count)];
                    Room room = new Room(p.x, p.y, s.width, s.height);
                    if (p.roomConnections.Count > 0) {
                        room._doorPositions = p.roomConnections;
                    }
                    //room.roomType = RoomType.HALL;
                    rooms.Add(room);
                } else {
                    break;
                }
            }
        }

        protected void place2x2RoomsConnected() {
            int _count = 0;
            while (true) {
                _count++;
                if (_count > 100) throw new Exception("Too deep cycle.");

                Size s = new Size(2, 2);
                List<Point> points = GetValidPoints(s).ConnectsWithAnyRoom(area, s, new ListRoom(rooms));
                if (points.Count > 0) {
                    Point p = points[Random.Range(0, points.Count)];
                    Room room = new Room(p.x, p.y, s.width, s.height);
                    if (p.roomConnections.Count > 0) {
                        room._doorPositions = p.roomConnections;
                    }                    
                    //room.roomType = RoomType.HALL;
                    rooms.Add(room);
                } else {
                    break;
                }
            }
        }

        protected void place1x1Rooms() {
            int _count = 0;
            while (true) {
                _count++;
                if (_count > 100) throw new Exception("Too deep cycle.");

                int roomWidth = 1;
                int roomHeight = 1;
                ListPoint points = GetValidPoints(roomWidth, roomHeight);
                if (points.Count > 0) {
                    Point p = points[Random.Range(0, points.Count)];
                    Room room = new Room(p.x, p.y, roomWidth, roomHeight);
                    room.roomType = RoomType.CORRIDOR;
                    rooms.Add(room);
                } else {
                    break;
                }
            }
        }
        
        protected ListPoint GetValidPoints(Size s) {
            return GetValidPoints(s.width, s.height);
        }

        protected ListPoint GetValidPoints(int roomWidth, int roomHeight) {
            foreach (Room rect in rooms) {
                for (int i = (int)rect.x; i < rect.x + rect.width; i++) {                    
                    for (int j = (int)rect.y; j < rect.y + rect.height; j++) {
                        if (rect.number == 0) throw new Exception("Room number can't be 0!");
                        area[i][j] = rect.number;
                    }                    
                }
            }

            

            ListPoint validPoints = new ListPoint();
            for (int i = 0; i < areaWidth; i++) {
                for (int j = 0; j < areaHeight; j++) {
                    if (area[i][j] != 0) {
                        continue;
                    }

                    bool isPointValid = true;
                    if (i + roomWidth > areaWidth || j + roomHeight > areaHeight) {
                        isPointValid = false;
                    } else {
                        for (int n = i; n < i + roomWidth; n++) {
                            for (int m = j; m < j + roomHeight; m++) {
                                if (area[n][m] != 0) {
                                    isPointValid = false;
                                    break;
                                }
                            }
                            if (!isPointValid) break;
                        }
                    }

                    if (isPointValid) {
                        validPoints.Add(new Point(i, j));
                    }
                }
            }

            return validPoints;
        }
    }
}


public interface IGenerator {
    List<Room> generateRooms();
    void generateNPC();
}

public class ApartamentGenerator : FillAlgorithm, IGenerator {
    public ApartamentGenerator() : base(16, 21) {

    }

    Room corridor;
    List<Room> appartments;

    public List<Room> generateRooms() {
        int count = 0;
        while (true) {
            if (count > 100) {
                throw new Exception("Too deep cycle.");
            }
            count++;

            rooms.Clear();

            List<Room> appartaments = new List<Room>();

            Size s = new Size(2, 21);
            corridor = placeRoom(s, GetValidPoints(s).GetEdgePointsBottom().xEquals(7));
            if (corridor == null) continue;

            corridor.roomType = RoomType.HALL;

            while (true) {
                Room appartmentRoom = PlaceApartment();
                if (appartmentRoom == null) break;
                appartaments.Add(appartmentRoom);
            }

            int _count = 0;
            while (true) {
                _count++;
                if (_count > 100) throw new Exception("Too deep cycle.");

                Size utilityRoomSize = new Size(3, 3);
                Room utilityRoom = placeRoom(utilityRoomSize, GetValidPoints(utilityRoomSize).ConnectsWithAnyRoom(area, utilityRoomSize, appartaments));
                if (utilityRoom == null) break;
            }

            Debug.Log("adasdasd");
            break;
        }
        

        //place3x3Rooms();
        //place2x2RoomsConnected();
        place2x2Rooms();
        place1x1Rooms();
        return rooms;
    }


    Room PlaceApartment() {
        Size s = new Size(4, 3);
        Room appartamentRoom = placeRoom(s, GetValidPoints(s).ConnectsWithRoom(area, s, corridor));
        if (appartamentRoom == null) return null;
        
        return appartamentRoom;
    }

    public void generateNPC() {
        
    }
}

public class BankGenerator : FillAlgorithm, IGenerator {

    private Room vaultRoom;
    private Room hallRoom;

    public BankGenerator(int width, int height) : base(width, height) {

    }

    public List<Room> generateRooms() {
        int count = 0;
        while (true) {
            if (count > 100) {
                throw new Exception("Too deep cycle.");
            }
            count++;

            rooms.Clear();

            Size s = new Size(8, 5);
            hallRoom = placeRoom(s, GetValidPoints(s).GetEdgePointsBottom());
            if (hallRoom == null) continue;
            hallRoom.roomType = RoomType.HALL;

            s = new Size(4, 4);
            Room officeRoom = placeRoom(s, GetValidPoints(s).ConnectsWithRoom(area, s, hallRoom));

            s = new Size(4, 7);
            vaultRoom = placeRoom(s, GetValidPoints(s).ConnectsWithRoom(area, s, officeRoom));
            if (vaultRoom == null) vaultRoom = placeRoom(s.flip(), GetValidPoints(s.flip()).ConnectsWithRoom(area, s.flip(), officeRoom));
            if (vaultRoom == null) continue;
            vaultRoom.noDoors = true;

            s = new Size(2, 4);
            Room toiletRoom = placeRoom(s, GetValidPoints(s).ConnectsWithRoom(area, s, officeRoom));
            if (toiletRoom == null) toiletRoom = placeRoom(s.flip(), GetValidPoints(s.flip()).ConnectsWithRoom(area, s.flip(), officeRoom));
            if (toiletRoom == null) continue;

            break;
        }

        place3x3Rooms();
        place2x2RoomsConnected();
        place2x2Rooms();
        place1x1Rooms();

        return rooms;
    }

    public void generateNPC() {
        Character npc = GameManager.Instance.CreateCharacter(vaultRoom.getRoomCenter() * Util.SCALE_FACTOR.x);
        npc.Create(new Game.Character());
        npc.SetGuardMode(vaultRoom);


        npc = GameManager.Instance.CreateCharacter(vaultRoom.getRoomCenter() * Util.SCALE_FACTOR.x);
        npc.Create(new Game.Character());

        
        npc = GameManager.Instance.CreateCharacter(hallRoom.getRoomCenter() * Util.SCALE_FACTOR.x);
        npc.SetGuardMode(hallRoom);
        Game.Character gameCharacter = new Game.Character();
        gameCharacter.role = "bankTeller";
        npc.Create(gameCharacter);
    }

}

public class BuildGenerator : MonoBehaviour {

	public List<Material> floor_material;
	Area area;

    IGenerator generator;
	bool generatingFinished = false;
	float delta = 0f;


    public int seed = -1;

	void Awake () {

	}

	void Start () {
        Util.clearGizmos();
        if (seed == 0) {
            seed = (int)System.DateTime.Now.Ticks;
        }
        //seed = -1951516824;
        //seed = 1889870988;
        //seed = -802846405;

        Random.seed = seed;
        Debug.Log(seed);


        Util.floor_material = floor_material;
        Grid level = new Grid(0f);
        Stage.Instance.stages.Add(level);
        Grid level2 = new Grid(2.99f);
        Stage.Instance.stages.Add(level2);
        Grid level3 = new Grid(2.99f * 2);
        Stage.Instance.stages.Add(level3);


        //TreeMap treeMap = new TreeMap ();
        //List<Rect> rects = treeMap.generateRooms ();
        generator = new ApartamentGenerator();
        List<Room> rooms = generator.generateRooms();
        int result = createLevel(rooms, level);

        /*
		int _c = 0;
		while (true) {
            _c++;
            if (_c > 20) {
                throw new Exception("Too deep cycle.");
            }

            Util.clearGizmos ();

			LSystem lsystem = new LSystem ();
			string seq0 = lsystem.iterate (3);
			string seq1 = lsystem.iterate (4);
			string seq2 = lsystem.iterate (4);

			if (!LSystem.isLSeqValid (seq2)) {
				continue;
			}
			Util.clearGizmos ();

			Area area = LSystem.createAreaFromLSeq (seq2);

			Area area2 = LSystem.createAreaFromLSeq (seq1);

			Area area3 = LSystem.createAreaFromLSeq (seq0, true);



			int result = createLevel (rects, level);
			Debug.Log (seq0);
			if (result == 1) {
				//createLevel (area2, level2);

				//createLevel (area3, level3);
				break;
			}

			
		}
        */

        enabled = false;

        transform.localScale = Util.SCALE_FACTOR;

        makePaths();
        generator.generateNPC();

        //generatingFinished = true;
	}

	void Update() {
		if (generatingFinished) {
			delta += Time.deltaTime;
			if (delta >= 3f) {
                
			}
		}
	}

    void makePaths() {
        AstarPath.active.Scan();
    }


	List<Room> createRooms(List<Rect> rects, Grid grid) {
		List<Room> rooms = new List<Room> ();
		for (int i = 0; i < rects.Count; i++) {
            rooms.Add (new Room(rects[i], grid));
		}

		return rooms;
	}

    int createLevel(List<Rect> rects, Grid level) {
        List<Room> rooms = createRooms(rects, level);
        return createLevel(rooms, level);
    }

	int createLevel (List<Room> rooms, Grid level) {
		foreach(Room room in rooms) {
            Util.addRectangle(new Rect(room.x, room.y, room.width, room.height));
            room.SetGrid(level);
        }

		for (int i = 0; i < rooms.Count; i++) {
			if (countOverlaps (rooms [i], rooms) > 0) {
				//return -1;
			}
			if (countOverlaps (rooms [i], rooms, true) == 0) {
				//return -1;
			}
		}



		List<Room> stack = new List<Room> ();



		/**********************************
			 * CREATE WALL SEGMENTS
			***********************************/
		foreach (Room room in rooms) {
			room.createFloor ();
			room.createSegments ();
		}




		/**********************************
		 * FIND INTERSECTIONS
		 ***********************************/
		for (int i = 0; i < rooms.Count; i++) {
			for (int j = 0; j < rooms.Count; j++) {
				if (i == j) {
					continue;
				}


				if (isRectsOverlapWith (rooms [i], rooms [j])) {

					List<Vector3> rect = createPerpindicularRect (rooms [i] [0], rooms [i] [1], 1);
					translateRoom (rect, 0.5f, 0.0f);


					for (int k = 0; k < rooms [i] [1].x - rooms [i] [0].x; k++) {
						if (isRectsOverlapWith (rooms [j], rect)) {
							rooms [i].getBottomWall ().setIntersection (k, true);
						}
						translateRoom (rect, 1.0f, 0.0f);
					}
					rect = createPerpindicularRect (rooms [i] [2], rooms [i] [3], 1);
					translateRoom (rect, -0.5f, 0.0f);
					for (int k = 0; k < rooms [i] [1].x - rooms [i] [0].x; k++) {
						if (isRectsOverlapWith (rooms [j], rect)) {
							rooms [i].getTopWall ().setIntersection (k, true);
						}
						translateRoom (rect, -1.0f, 0.0f);
					}


					rect = createPerpindicularRect (rooms [i] [1], rooms [i] [2], 1);
					translateRoom (rect, 0.0f, 0.5f);
					for (int k = 0; k < rooms [i] [2].z - rooms [i] [1].z; k++) {

						if (isRectsOverlapWith (rooms [j], rect)) {
							rooms [i].getRightWall ().setIntersection (k, true);

						}
						translateRoom (rect, 0.0f, 1.0f);
					}

					rect = createPerpindicularRect (rooms [i] [3], rooms [i] [0], -1);
					translateRoom (rect, 0.0f, -0.5f);
					for (int k = 0; k < rooms [i] [2].z - rooms [i] [1].z; k++) {
						if (isRectsOverlapWith (rooms [j], rect)) {
							rooms [i].getLeftWall ().setIntersection (k, true);
						}
						translateRoom (rect, 0.0f, -1.0f);
					}

				}
			}
		}
        foreach (Room room in rooms) {
            foreach (Wall wall in room.walls) {
                for (int i = 0; i < wall.length / Util.CELL_SIZE; i++) {
                    if (wall.segments[i].intersect) {
                        if (wall.direction == Wall.TOP) {
                            Room roomAbove = wall.segments[i].getCellAbove().parentRoom;
                            wall.segments[i] = roomAbove.getBottomWall().segments[(room.x - roomAbove.x) + (room.width - 1) - i];
                            continue;
                        }

                        if (wall.direction == Wall.LEFT) {
                            Room roomLeft = wall.segments[i].getCellLeft().parentRoom;
                            //wall.segments[i].getCellRight().drawCubeGizmo();
                            wall.segments[i] = roomLeft.getRightWall().segments[(room.y - roomLeft.y) + (room.height - 1) - i];

                            continue;
                        }
                    }
                }
            }
        }

        
        foreach (Room room in rooms) {
            if (room.roomType == RoomType.CORRIDOR) {
                
                ListSegment segs = room.getRoomSegments();
                foreach (Segment seg in room.getRoomSegments().getIntersectSegments()) {
                    seg.noDoor = true;
                }

                int bottom = Random.Range(0, 2);
                bottom = 1;
                if (bottom == 1) {
                    if (room.getBottomRoom() != null) {
                        room.getBottomWall().segments[0].skip = true;
                        room.ownedBy = room.getBottomWall().segments[0].getCellBelow().parentRoom;
                        room.getBottomWall().segments[0].getCellAbove().drawCubeGizmo(new Color(0, 1, 0, 0.5f));

                    } else {
                        room.getTopWall().segments[0].skip = true;
                        room.getTopRoom().getBottomWall().segments[0].getCellAbove().drawCubeGizmo(new Color(0, 0, 1, 0.5F));
                        room.ownedBy = room.getTopRoom().getBottomWall().segments[0].getCellAbove().parentRoom;
                    }
                } else {
                    if (room.getRightRoom() != null) {
                        room.getRightWall().segments[0].skip = true;
                        room.ownedBy = room.getRightWall().segments[0].getCellRight().parentRoom;
                    } else {
                        room.getLeftWall().segments[0].skip = true;
                        room.ownedBy = room.getLeftWall().segments[0].getCellRight().parentRoom;
                    }
                }

                if (room.getLeftRoom() != null && (room.getLeftRoom() == room.ownedBy || room.getLeftRoom().ownedBy == room.ownedBy)) {
                    room.getLeftWall().segments[0].skip = true;
                }

                if (room.getRightRoom() != null && (room.getRightRoom() == room.ownedBy || room.getRightRoom().ownedBy == room.ownedBy)) {
                    room.getRightWall().segments[0].skip = true;
                }

                if (room.getTopRoom() != null && (room.getTopRoom() == room.ownedBy || room.getTopRoom().ownedBy == room.ownedBy)) {
                    room.getTopWall().segments[0].skip = true;
                }

                if (room.getBottomRoom() != null && (room.getBottomRoom() == room.ownedBy || room.getBottomRoom().ownedBy == room.ownedBy)) {
                    room.getBottomWall().segments[0].skip = true;
                }
            }
        }

        
        foreach (Room room in rooms) {
            if (room.roomType == RoomType.CORRIDOR) {
                if (room.getRightRoom() != null) {
                    if (room.getBottomRoom() != null && room.getBottomRoom().roomType != RoomType.CORRIDOR) {
                        continue;
                    }
                    if (room.getRightRoom().roomType == RoomType.CORRIDOR && room.ownedBy != room.getRightRoom().ownedBy) {
                        room.getBottomWall().segments[0].skip = false;
                        continue;
                    }
                    if (room.getTopRoom() != null && room.getTopRoom().ownedBy == null && room.getRightRoom().ownedBy == null) {
                        if (room.getLeftRoom() != null && room.getLeftRoom().roomType == RoomType.CORRIDOR) {
                            continue;
                        }
                    }

                    room.ownedBy = room.getRightRoom();
                    room.getRightWall().segments[0].skip = true;

                    if (room.getBottomRoom() != null) {
                        if (room.getBottomRoom().getBottomRoom() != null && room.getBottomRoom().getBottomRoom().roomType != RoomType.CORRIDOR) {
                            room.getBottomWall().segments[0].skip = false;
                        }

                        if (room.getBottomRoom().getRightRoom() != null && room.getRightRoom() != room.getBottomRoom().getRightRoom()) {
                            room.getBottomWall().segments[0].skip = false;

                        }

                    }
                }
            }
        }

        DoorGenerator doorGen = new DoorGenerator (rooms, level);
		doorGen.makeDoors ();

		WindowGenerator winGen = new WindowGenerator (new ListRoom(rooms));
		winGen.makeWindows ();

		makeRooms (rooms);


		foreach (Room r in rooms) {
			List<Segment> segs = r.getTopWall().segments;
			foreach (Segment seg in segs) {
				//Destroy (seg.wallObj);
			}
		}

		//makeRoads (rooms);


		return 1;
	}



	//Rect area = new Rect (0, 0, 20, 20);

	Vector2 getFreePoint() {
		for (int y = (int)area.minZ - 1; y < (int)area.maxZ + 1; y++) {
			for (int x = (int)area.minX - 1; x < (int)area.maxX + 1; x++) {
				if (area.isPointInside (new Vector3 (x, 0, y)) && Stage.Instance.stages [0].getCellByPos (x, y).parentRoom == null) {
					if (area.isPointInside ( new Vector3(x + 1, 0, y) ) && area.isPointInside ( new Vector3(x, 0, y + 1) )) {

						return new Vector2 (x, y);
					}
				}
			}
		}
		return Util.VECTOR3_NULL;
	}






	/**********************************
		* MAKE ROADS
		***********************************/
	void makeRoads (List<Room> rooms) {
		Grid stage0 = Stage.Instance.stages [0];
		Rect rect = getFullSquare(rooms);

		Cell cell = stage0.getCellByPos (rect.x - Util.CELL_SIZE * 11, rect.y - Util.CELL_SIZE * 1);
		GameObject stripeUp = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Generator/Road/IntersectionStripe"));
		placeItem(stripeUp, cell, 0, 0, 90, 0);

		cell = cell.getCellToTheLeft ().getCellToTheLeft ().getCellAbove ().getCellAbove();
		for (int i = 0; i < 20; i++) {
			GameObject road = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Generator/Road/RoadCombined"));

			placeItem (road, cell, 0, 0, 90, 0);
			cell = cell.getCellAbove ().getCellAbove ();
		}


		cell = stage0.getCellByPos (rect.x-Util.CELL_SIZE*10, rect.y-Util.CELL_SIZE*11);
		GameObject item = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Generator/Road/Intersection"));
		placeItem(item, cell);

		cell = stage0.getCellByPos (rect.x-Util.CELL_SIZE*2, rect.y-Util.CELL_SIZE*13);

		GameObject roadIntersection = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Generator/Road/RoadIntersectionCombined"));
		placeItem (roadIntersection, cell, 0, 0, 0, 0);
		cell = cell.getCellToTheRight ().getCellToTheRight();

		for (int i = 0; i < 20; i++) {
			GameObject road = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Generator/Road/RoadCombined"));

			placeItem (road, cell, 0, 0, 0, 0);
			cell = cell.getCellToTheRight ().getCellToTheRight();
		}




	}

	/**********************************
		 * DRAW ROOMS
		***********************************/
	void makeRooms(List<Room> rooms) {

		for (int i = 0; i < rooms.Count; i++) {
			rooms[i].drawWalls();
			rooms[i].drawFloor();

			Floor floor = rooms[i].floor;


			//Util.addGizmo(rooms[i].getRoomCenter(), rooms[i].number);
			/*
				if (i == 1) {

					GameObject item = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Furniture/Cashdesk"));
					placeItem(item, floor.getCell(floor.width, floor.height), 0.09f, 0.15f, 90);
					
					//item.layer = LayerMask.NameToLayer ("Obstacle");
				}

				if (i == 3) {
					GameObject item = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Bath/Toilet"));
					placeItem(item, floor.getCell(floor.width, floor.height), 0.03f, 0.03f, 90);

					//item.layer = LayerMask.NameToLayer ("Obstacle");
				}
				
				if (i == 4) {
					GameObject item = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Furniture/Safe"));
					placeItem(item, floor.getCell(floor.width, floor.height), 0.09f, 0.15f, 90);

					item = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Furniture/Safe"));
					placeItem(item, floor.getCell(floor.width, floor.height-1), 0.09f, 0.15f, 90);
					//item.layer = LayerMask.NameToLayer ("Obstacle");
				}
				*/

			/*
			if (1 == 1) {
				Furniture4x3 f = Furniture.find4x3 (floor.getCellsList (), 0);
				if (f != null) {
					f.occupy (true);
					GameObject item = (GameObject)UnityEngine.Object.Instantiate (Resources.Load ("Prefabs/Generator/Staircase"));
					placeItem (item, f.startCell, 0.09f, 0.15f, 0);

					f.drawGizmo ();
				} else {
					Debug.Log ("YAHOOO");
				}
			}
			*/

            /*
			if (rooms [i].roomType != RoomType.CORRIDOR) {
				continue;
			}
            */


            /*
			List<int> angles = new List<int> ();
			angles.Add (0); angles.Add (90); angles.Add (180);

			List<Cell> cells = floor.getCellsList ();
			cells.Shuffle ();
			foreach (Cell cell in cells) {
				int  angle = angles.SelectRandom ();
				Furniture2x1 f = new Furniture2x1 (cell, angle);
				if (f.isAlreadyOccupied () == false && f.inside ()) {
					f.occupy (true);


					GameObject item = (GameObject)UnityEngine.Object.Instantiate (Resources.Load ("Prefabs/Furniture/Table"));
					placeItem (item, cell, 0.09f, 0.15f, angle);
				}
			}
            */
		}

	}


	void placeItem(GameObject item, Cell cell, float dX=0f, float dZ=0f, int angle=0, float dY = 0) {
		if (angle == 0) {
			item.transform.position = cell.getWorldPosition (dX, -dZ) + new Vector3(0, dY, 1) * Util.CELL_SIZE;
		} else if (angle == 90) {
			item.transform.position = cell.getWorldPosition (-dZ, -dX) + new Vector3(1, dY, 1) * Util.CELL_SIZE;;
		} else if (angle == 180) {
			item.transform.position = cell.getWorldPosition (-dX, dZ) + new Vector3(1, dY, 0) * Util.CELL_SIZE;
		} else if (angle == 270) {
			item.transform.position = cell.getWorldPosition (dZ, dX);
		}
		item.transform.Rotate (0, angle, 0);
	}


	public void OnDrawGizmos() {
		foreach (GameGizmo giz in TextGizmo.Instance.gizmos) {
			TextGizmo.Instance.DrawText (Camera.main, giz.pos, giz.message);
		}

		foreach (CubeGizmo giz in TextGizmo.Instance.cubeGizmos) {
			Gizmos.color = giz.color;
			Gizmos.DrawCube(giz.pos, new Vector3(1, 1, 1));
		}


		foreach (Line line in TextGizmo.Instance.lines) {
			Debug.DrawLine (line.start, line.end, line.color);
		}

	}



	List<Vector3> createPerpindicularRect(Vector3 a, Vector3 b, int f) {
		//Vector3 perpendicular = Util.perpendicular (a, b, f);
		Vector3 dir = a - b; 
		Vector3 perpendicular = new Vector3(-dir.z, 0.0f, dir.x) / (float)Math.Sqrt(dir.x*dir.x + dir.z*dir.z) * 0.5f * f;

		List<Vector3> rect = new List<Vector3>();
		rect.Add(a-perpendicular);
		rect.Add(a+perpendicular);
		rect.Add(a+perpendicular+new Vector3(0, 0, 0.1f));
		rect.Add(a-perpendicular+new Vector3(0, 0, 0.1f));

		return rect;
	}


	Vector3 normDirVector(Vector3 v) {
		float ratio = v.x / v.z;
		if (ratio >= 0.75 && ratio <= 1.25) {
			return new Vector3 (v.x > 0.0f ? 1.0f : -1.0f, 0.0f, v.z > 0.0f ? 1.0f : -1.0f);
		}
		if (ratio <= 0.75) {
			return new Vector3 (0.0f, 0.0f, v.z > 0.0f ? 1.0f : -1.0f);
		}
		return new Vector3 (v.x > 0.0f ? 1.0f : -1.0f, 0.0f, 0.0f);
	}

	Rect getFullSquare(List<Room> rooms) {
		float minX = 99999;
		float minZ = 99999;
		float maxX = -99999;
		float maxZ = -99999;
		for (int i = 0; i < rooms.Count; i++) {
			if (rooms[i][0].x < minX) {
				minX = rooms[i][0].x;
			}
			if (rooms[i][0].z < minZ) {
				minZ = rooms[i][0].z;
			}

			if (rooms[i][2].x > maxX) {
				maxX = rooms[i][2].x;
			}
			if (rooms[i][2].z > maxZ) {
				maxZ = rooms[i][2].z;
			}
		}
		Rect rect = new Rect (minX, minZ, maxX - minX, maxZ - minZ);
		return rect;
	}




	Vector3 getGeometricCenter(List<Room> rooms) {
		Rect square = getFullSquare (rooms);
		return new Vector3 (square.center.x, 0.0f, square.center.y);
	}


	void translateRoom(List<Vector3> room, float dX, float dZ) {
		for (int i = 0; i < room.Count; i++) {
			room[i] = new Vector3 (room[i].x + dX, 0.0f, room[i].z + dZ);
		}
	}

	int countOverlaps(List<Vector3> room, List<Room> rooms, bool with = false) {
		int count = 0;
		for (int i = 0; i < rooms.Count; i++)
		{
			bool overlaps = with ? isRectsOverlapWith(room, rooms[i]) : isRectsOverlap(room, rooms[i]);
			if (room != rooms[i] && overlaps) {
				count++;
			}
		}
		return count;
	}



	bool isRectsOverlap(List<Vector3> r1, List<Vector3> r2) {
		bool xOverlap = Util.valueInRange(r1[0].x, r2[0].x, r2[1].x) || Util.valueInRange(r2[0].x, r1[0].x, r1[1].x);
		bool yOverlap = Util.valueInRange(r1[1].z, r2[1].z, r2[2].z) || Util.valueInRange(r2[1].z, r1[1].z, r1[2].z);
		return xOverlap && yOverlap;
	}



	bool isRectsOverlapWith(List<Vector3> r1, List<Vector3> r2) {
		bool xOverlap = Util.valueInRangeInclusive(r1[0].x, r2[0].x, r2[1].x) || Util.valueInRangeInclusive(r2[0].x, r1[0].x, r1[1].x);
		bool yOverlap = Util.valueInRangeInclusive(r1[1].z, r2[1].z, r2[2].z) || Util.valueInRangeInclusive(r2[1].z, r1[1].z, r1[2].z);
		return xOverlap && yOverlap;
	}

}




public class TreeMap {
    public TreeMap() {



    }

    public List<Rect> generateRooms() {
        const double MinSliceRatio = 0.35;

        int width = Random.Range(10, 15);
        int height = Random.Range(10, 15);

        var elements = new int[Random.Range(6, 7)];
        for (int i = 0; i < elements.Length; i++) {
            elements[i] = Random.Range(25, 80);
        }

        Debug.Log(elements);

        var sorted = elements
            .Select(x => new Element<string> { Object = x.ToString(), Value = x })
            .OrderByDescending(x => x.Value)
            .ToList();

        var slice = GetSlice(sorted, 1, MinSliceRatio);

        var rectangles = GetRectangles(slice, width, height)
            .ToList();


        List<Rect> rects = new List<Rect>();
        for (int i = 0; i < rectangles.Count; i++) {
            Rect rect = new Rect(rectangles[i].X, rectangles[i].Y, rectangles[i].Width, rectangles[i].Height);
            rects.Add(rect);
        }
        return rects;
    }

    public Slice<T> GetSlice<T>(IEnumerable<Element<T>> elements, double totalSize, double sliceWidth) {
        if (!elements.Any()) return null;
        if (elements.Count() == 1) return new Slice<T> { Elements = elements, Size = totalSize };

        var sliceResult = GetElementsForSlice(elements, sliceWidth);

        return new Slice<T> {
            Elements = elements,
            Size = totalSize,
            SubSlices = new[] {
                GetSlice(sliceResult.Elements, sliceResult.ElementsSize, sliceWidth),
                GetSlice(sliceResult.RemainingElements, 1 - sliceResult.ElementsSize,
                    sliceWidth)
            }
        };
    }

    private SliceResult<T> GetElementsForSlice<T>(IEnumerable<Element<T>> elements, double sliceWidth) {
        var elementsInSlice = new List<Element<T>>();
        var remainingElements = new List<Element<T>>();
        double current = 0;
        double total = elements.Sum(x => x.Value);

        foreach (var element in elements) {
            if (current > sliceWidth) {
                remainingElements.Add(element);
            } else {
                elementsInSlice.Add(element);
                current += element.Value / total;
            }
        }

        return new SliceResult<T> {
            Elements = elementsInSlice,
            ElementsSize = current,
            RemainingElements = remainingElements
        };
    }

    public class SliceResult<T> {
        public IEnumerable<Element<T>> Elements { get; set; }
        public double ElementsSize { get; set; }
        public IEnumerable<Element<T>> RemainingElements { get; set; }
    }

    public class Slice<T> {
        public double Size { get; set; }
        public IEnumerable<Element<T>> Elements { get; set; }
        public IEnumerable<Slice<T>> SubSlices { get; set; }
    }

    public class Element<T> {
        public T Object { get; set; }
        public double Value { get; set; }
    }


    public IEnumerable<SliceRectangle<T>> GetRectangles<T>(Slice<T> slice, int width,
        int height) {
        var area = new SliceRectangle<T> { Slice = slice, Width = width, Height = height };

        foreach (var rect in GetRectangles(area)) {
            // Make sure no rectangle go outside the original area
            if (rect.X + rect.Width > area.Width) rect.Width = area.Width - rect.X;
            if (rect.Y + rect.Height > area.Height) rect.Height = area.Height - rect.Y;

            yield return rect;
        }
    }

    private IEnumerable<SliceRectangle<T>> GetRectangles<T>(
        SliceRectangle<T> sliceRectangle) {
        var isHorizontalSplit = sliceRectangle.Width >= sliceRectangle.Height;
        var currentPos = 0;
        foreach (var subSlice in sliceRectangle.Slice.SubSlices) {
            var subRect = new SliceRectangle<T> { Slice = subSlice };
            int rectSize;

            if (isHorizontalSplit) {
                rectSize = (int)Math.Round(sliceRectangle.Width * subSlice.Size);
                subRect.X = sliceRectangle.X + currentPos;
                subRect.Y = sliceRectangle.Y;
                subRect.Width = rectSize;
                subRect.Height = sliceRectangle.Height;
            } else {
                rectSize = (int)Math.Round(sliceRectangle.Height * subSlice.Size);
                subRect.X = sliceRectangle.X;
                subRect.Y = sliceRectangle.Y + currentPos;
                subRect.Width = sliceRectangle.Width;
                subRect.Height = rectSize;
            }

            currentPos += rectSize;

            if (subSlice.Elements.Count() > 1) {
                foreach (var sr in GetRectangles(subRect))
                    yield return sr;
            } else if (subSlice.Elements.Count() == 1)
                yield return subRect;
        }
    }

    public class SliceRectangle<T> {
        public Slice<T> Slice { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
    }
}