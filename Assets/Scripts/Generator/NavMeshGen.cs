﻿using RAIN.Navigation.NavMesh;
using System.Collections.Generic;
using System.Collections;
using UnityEngine;
using RAIN.Navigation;
using RAIN.Navigation.Targets;
using RAIN.Core;
using RAIN.BehaviorTrees;
using RAIN.Minds;
using RAIN.Memory;
using RAIN;

public class NavMeshGen : MonoBehaviour
{
	private int _threadCount = 4;
	NavMeshRig tRig;
	private bool isNavMeshDone = false;



	void Start()
	{
		


		// create NavMesh
		tRig = gameObject.GetComponentInChildren<RAIN.Navigation.NavMesh.NavMeshRig>();
		StartCoroutine(GenerateNavmesh());

		// NavMesh ready ???
		StartCoroutine(navMeshCheck());


	}

	// This will regenerate the navigation mesh when called
	IEnumerator GenerateNavmesh()
	{
		// Unregister any navigation mesh we may already have (probably none if you are using this)
		tRig.NavMesh.UnregisterNavigationGraph();
		tRig.NavMesh.Size = 100;
		float startTime = Time.time;
		tRig.NavMesh.StartCreatingContours(tRig, _threadCount);
		while (tRig.NavMesh.Creating)
		{
			tRig.NavMesh.CreateContours();

			yield return new WaitForSeconds(1);
		}
		isNavMeshDone = true;
		float endTime = Time.time;
		Debug.Log("NavMesh generated in " + (endTime - startTime) + "s");
		gameObject.GetComponentInChildren<RAIN.Navigation.NavMesh.NavMeshRig>().NavMesh.RegisterNavigationGraph();
		gameObject.GetComponentInChildren<RAIN.Navigation.NavMesh.NavMeshRig>().Awake();

	}

	IEnumerator navMeshCheck()
	{

		yield return new WaitForSeconds(1);
		if (isNavMeshDone)
		{
			Spawn();
		}
		else { StartCoroutine(navMeshCheck()); }

	}

	private void Spawn()
	{
		


	}
}