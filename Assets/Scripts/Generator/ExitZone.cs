﻿using UnityEngine;
using System.Collections;

public class ExitZone : MonoBehaviour {

	void OnTriggerEnter(Collider other) {

        Character character = other.GetComponent<Character>();
        if (character != null && character.controlledByPlayer) {
            Application.LoadLevel("Main");
        }
        
    }
    
}
