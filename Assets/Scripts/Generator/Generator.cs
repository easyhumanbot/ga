﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;
using Pathfinding;

using Random = UnityEngine.Random;

using LSystemAlgorithm;

public static class EnumerableExtension
{

	private static System.Random random = new System.Random();

	public static T SelectRandom<T>(this IEnumerable<T> sequence)
	{

		if(sequence == null)
		{
			throw new ArgumentNullException();
		}

		if (!sequence.Any())
		{
			throw new ArgumentException("The sequence is empty.");
		}

		ICollection<T> col = (ICollection<T>)sequence;
		int i = Random.Range(0, col.Count);
		return col.ElementAt (i);
	}

	public static void Shuffle<T>(this IList<T> list)  
	{  
		var rnd = new System.Random ();

		int _c = 0;
		int n = list.Count;  
		while (n > 1) {
            _c++;
            if (_c > 500) {
                throw new Exception("Too long cycle.");
            }

            n--;  
			int k = rnd.Next(0, n + 1);  
			T value = list[k];  
			list[k] = list[n];  
			list[n] = value;  
		}  
	}
}




namespace LevelGenerator {

	public class Area {
		private List<Vector3> vertices;

		public float minX;
		public float minZ;

		public float maxX;
		public float maxZ;

		private List<Vector3> roomVertices = new List<Vector3>();

		public Area(List<Vector3> _vertices) {
			vertices = _vertices;

			if (vertices.Count < 3) {
				throw new Exception ("Area must contain at least 3 points.");
			}


			minX = vertices [0].x;
			minZ = vertices [0].z;

			maxX = vertices [0].x;
			maxZ = vertices [0].z;

			for (int i = 0; i < vertices.Count; i++) {
				Vector3 v = vertices [i];

				if (v.x < minX) {
					minX = v.x;
				}

				if (v.z < minZ) {
					minZ = v.z;
				}

				if (v.x > maxX) {
					maxX = v.x;
				}

				if (v.z > maxZ) {
					maxZ = v.z;
				}

				//Util.addCubeGizmo (v, Color.magenta);
				//Util.addGizmo (v, i);
			}


		}

		public bool isRectInside(Rect r) {
			Vector3 s = new Vector3 (r.x, 0, r.y);
			if (!IsPointInPolygon4 (s)) {				
				return false;
			}
			if (!IsPointInPolygon4 (s + new Vector3 (r.width, 0, r.height))) {
				return false;
			}
			if (!IsPointInPolygon4 (s + new Vector3 (r.width, 0, 0)))
				return false;
			if (!IsPointInPolygon4 (s + new Vector3 (0, 0, r.height)))
				return false;
			return true;
		}

		public bool IsPointInPolygon4(Vector3 pnt)
		{
			Vector3[] poly = vertices.ToArray ();

			int i, j;
			int nvert = poly.Length;
			bool c = false;
			for (i = 0, j = nvert - 1; i < nvert; j = i++)
			{
				if (((poly[i].z > pnt.z) != (poly[j].z > pnt.z)) &&
					(pnt.x < (poly[j].x - poly[i].x) * (pnt.z - poly[i].z) / (poly[j].z - poly[i].z) + poly[i].x))
					c = !c; 
			}
			return c;
		}


		public List<Rect> getRectangles() {
			List<Rect> rectangles = new List<Rect> ();

			foreach (Vector3 v in vertices) {
				Rect r = new Rect (v.x, v.z, 1, 1);

                int c = 0;
				do {
                    c++;
                    if (c > 500) throw new Exception("Too deep cycle.");

					r = new Rect (r.position.x, r.position.y, r.width, r.height + 1);
				} while (isRectInside (r));
				r = new Rect (r.position.x, r.position.y, r.width, r.height - 1);

                c = 0;
				do  {
                    c++;
                    if (c > 500) throw new Exception("Too deep cycle.");

                    r = new Rect (r.position.x, r.position.y, r.width + 1, r.height);
				} while (isRectInside (r));

				r = new Rect (r.position.x, r.position.y, r.width, r.height + 1);

				rectangles.Add (r);
				break;
			}

			return rectangles;
		}

		public List<Rect> getRectanglesOld() {
			List<Rect> rectangles = new List<Rect> ();

			for (int i = vertices.Count - 1; i >=0; i--) {
				int start = i;
				int end = Util._index (i + 1, vertices.Count);
				Vector3 dir = (vertices [end] - vertices [start]).normalized;
				if (dir == Vector3.zero) {
					vertices.RemoveAt (i);
				}
			}

			for (int i = 0; i < vertices.Count; i++) {
				int start = i;
				int prev = Util._index (i - 1, vertices.Count);
				int end = Util._index (i + 1, vertices.Count);
				int end2 = Util._index (i + 2, vertices.Count);

				Vector3 dir = (vertices [end] - vertices [start]).normalized;
				Vector3 norm = Vector3.Cross (dir, Vector3.down).normalized;	

				Vector3 dir2 = (vertices [end2] - vertices [end]).normalized;

				Vector3 dir0 = (vertices [start] - vertices [prev]).normalized;
				Vector3 norm0 = Vector3.Cross (dir0, Vector3.down).normalized;
				if (dir != dir0 && dir != norm0) {

					//Util.addLine (vertices[start], vertices[start] + dir, Color.cyan);
					//Util.addLine (vertices[prev], vertices[prev] + dir0, Color.red);
					//Util.addLine (vertices[prev], vertices[prev] + dir0, Color.black);

					Vector3 vert1 = vertices[start];
					Vector3 vert2 = _findVerticeInDirection (vertices[start], norm0);
					if (vert2 == Util.VECTOR3_NULL) {
						continue;
					}

					roomVertices.Add (vert1);
					roomVertices.Add (vert2);

					Util.addLine (vert1, vert2, Color.green);


					Vector3 _n = Vector3.Cross (dir, Vector3.up).normalized;
					Vector3 vert3 = _findVerticeInDirection (vert2, _n);

					_n = Vector3.Cross (_n, Vector3.up).normalized;
					Vector3 vert4 = _findVerticeInDirection (vert3, _n);





					Rect r = new Rect (vert3.x, vert1.z, vert1.x - vert3.x, vert3.z - vert1.z);
					rectangles.Add (SRect.normalizeRect(r));
				}
			}





			List<Vector3> lastRectVertices = new List<Vector3> ();

			for (int i = 0; i < vertices.Count; i++) {
				int start = i;
				int next = Util._index (i + 1, vertices.Count);

				Vector3 middle = (vertices [next] - vertices [start]) / 2 + vertices[start];

				bool isInside = false;
				foreach (Rect r in rectangles) {

					if (SRect.containsWith(r, middle)) {
						isInside = true;
					}
				}

				if (!isInside) {
					lastRectVertices.Add (vertices [start]);
					lastRectVertices.Add (vertices [next]);
				}

			}



			Vector3 vmin = lastRectVertices [0];
			Vector3 vmax = lastRectVertices [0];
			foreach (Vector3 v in lastRectVertices) {
				if (v.x < vmin.x)
					vmin = v;
				if (v.z < vmin.z)
					vmin = v;
				if (v.x > vmax.x)
					vmax = v;
				if (v.z > vmax.z)
					vmax = v;
			}


			rectangles.Add (new Rect (vmin.x, vmin.z, vmax.x - vmin.x, vmax.z - vmin.z));

			return rectangles;
		}

		private Vector3 _findVerticeInDirectionLine(Vector3 a1, Vector3 direction) {
			Vector3 a2 = a1 += direction;
			for (int i = 0; i < 50; i++) {

				a2 += direction;
				for (int j = 0; j < vertices.Count; j++) {
					//Util.addCubeGizmo (vertice, Color.black);

					for (int e = 0; e < vertices.Count; e++) {
						Vector3 b0 = vertices [Util._index (e - 1, vertices.Count)];
						Vector3 b1 = vertices [e];
						Vector3 b2 = vertices [Util._index (e + 1, vertices.Count)];
						Vector3 b3 = vertices [Util._index (e + 2, vertices.Count)];

						if (Vector3.Cross (a1 - a2, b1 - b2) == Vector3.zero) {
							continue;
						}



						Vector3 intersection;
						if (Util.linesIntersect (a1, a2, b1, b2, out intersection)) {
							Util.addGizmo (b1, "b1");
							Util.addGizmo (b2, "b2");

							if ((b3 - b2).normalized == (b2 - b1).normalized) {
								Util.addLine (b2, b2 + (b3 - b2).normalized, Color.red);
								return intersection;
							}
						}
					}

				}
			}
			return Util.VECTOR3_NULL;
		}

		private Vector3 _findVerticeInDirection(Vector3 a1, Vector3 direction) {
			Vector3 a2 = a1 += direction;
			for (int i = 0; i < 50; i++) {

				a2 += direction;
				for (int j = 0; j < vertices.Count; j++) {
					if (a2 == vertices [j]) {
						return a2;
					}

				}
			}
			return Util.VECTOR3_NULL;
		}



		public bool isRoomInside(Room room) {
			if (!isPointInside (room [0])) {
				return false;
			}
			if (!isPointInside (room [1])) {
				return false;
			}
			if (!isPointInside (room [2])) {
				return false;
			}
			if (!isPointInside (room [3])) {
				return false;
			}
			return true;
		}



		public bool isPointInside(Vector3 point1) {
			int countIntersections = 0;

			Vector3 point2 = point1 + Vector3.forward * 2000;

			for (int i = 0; i < vertices.Count; i++) {
				int start = i;
				int end = i + 1;

				if (i == vertices.Count - 1) {
					start = i;
					end = 0;
				}

				if (Util.linesIntersect (vertices[start], vertices[end], point1, point2)) {
					countIntersections++;
				}
			}

			if (countIntersections % 2 == 1) {
				return true;
			}
			return false;
		}
	}

	public class Stage {
		#region Singleton
		private class Singleton
		{
			static Singleton()
			{
				if (instance == null)
				{
					instance = new Stage();
				}
			}

			internal static readonly Stage instance;
		}

		public static Stage Instance { get { return Singleton.instance; } }

		#endregion

		public List<Grid> stages;

		private Stage() {
			stages = new List<Grid> ();
		}


	}


	public class Grid : List<List<Cell>> {

		public int size = 60;

		public float height;

		public Grid(float height) {
			this.height = height;

			for (int i = -size; i < size; i++) {
				List<Cell> row = new List<Cell>();
				for (int j = -size; j < size; j++) {
					var cell = new Cell (i, j, this);
					row.Add (cell);

				}
				this.Add (row);
			}
		}

		public Cell getCellByPos(Vector3 pos) {
			return getCellByPos (pos.x, pos.z);
		}

		public Cell getCellByPos(float x, float z) {


			int i = (int)Math.Floor(x) / (int)Util.CELL_SIZE;
			int j = (int)Math.Floor(z) / (int)Util.CELL_SIZE;;


			return this [i+size] [j+size];
		}
	}


	public static class SRect {

		public static bool containsWith(Rect r, Vector3 p) {
			float d = 0.1f;

			float w2 = r.width / 2f;
			float h2 = r.height / 2f;

			bool xRange = Util.valueInRangeInclusive (p.x, r.center.x - w2 - d, r.center.x + w2 + d);
			bool yRange = Util.valueInRangeInclusive (p.z, r.center.y - h2 - d, r.center.y + h2 + d);

			return xRange && yRange;
		}


		public static Rect normalizeRect(Rect r) {
			float x = r.x;
			float y = r.y;
			float width = r.width;
			float height = r.height;



			if (r.width < 0) {
				x = r.x + r.width;
				width = r.width * -1;
			}
			if (r.height < 0) {
				y = r.y + r.height;
				height = r.height * -1;
			}
			return new Rect (x, y, width, height);
		}
	}

	public static class Util {


		public static Vector3 VECTOR3_NULL = new Vector2 (-999, -999);

		public static float CELL_SIZE = 1.0f;
        public static Vector3 SCALE_FACTOR = new Vector3(1.6f, 1.1f, 1.6f);

		public static List<Material> floor_material;

		public static int _index(int index, int cnt) {
			if (index == -1) {
				return cnt - 1;
			}
			if (index < cnt) {
				return index;
			}
			return index % cnt;
		}

		public static bool valueInRange(float value, float min, float max) {
			return (value > min) && (value < max);
		}

		public static bool valueInRangeInclusive(float value, float min, float max) {
			return (value >= min) && (value <= max);
		}


		public static void addLine(Vector3 start, Vector3 end) {
			Util.addLine (start, end, Color.blue);
		}

		public static void addLine(Vector3 start, Vector3 end, Color color) {
			Line line = new Line (start, end);
			line.color = color;

			TextGizmo.Instance.lines.Add (line);
		}

		public static void addRectangle(Rect r) {
			Color color = Color.yellow;

			float y = 0.2f;

			Vector3 pos = new Vector3 (r.position.x, y, r.position.y);
			Util.addLine (pos, pos + new Vector3(r.width, 0, 0), color);
			Util.addLine (pos + new Vector3(r.width, 0, 0), pos + new Vector3(r.width, 0, r.height), color);
			Util.addLine (pos + new Vector3(r.width, 0, r.height), pos + new Vector3(0, 0, r.height), color);
			Util.addLine (pos + new Vector3(0, 0, r.height), pos, color);
		}

		private static Vector2 v3tov2(Vector3 v3) {
			return new Vector2 (v3.x, v3.z);
		}


		public static bool linesIntersect(Vector3 p1, Vector3 p2, Vector3 q1, Vector3 q2) {
			Vector2 intersection;
			bool doIntersect = _LineIntersection (Util.v3tov2 (p1), Util.v3tov2 (p2), Util.v3tov2 (q1), Util.v3tov2 (q2), out intersection);

			if (doIntersect) {
				return true;
			}

			return false;
		}


		public static bool linesIntersect(Vector3 p1, Vector3 p2, Vector3 q1, Vector3 q2, out Vector3 intersection) {
			Vector2 intersect;
			bool doIntersect = _LineIntersection (Util.v3tov2 (p1), Util.v3tov2 (p2), Util.v3tov2 (q1), Util.v3tov2 (q2), out intersect);

			intersection = Util.VECTOR3_NULL;
			if (doIntersect) {
				intersection = new Vector3 (intersect.x, 0f, intersect.y);
				return doIntersect;
			} 

			return false;
		}



		private static bool _LineIntersection( Vector2 p1,Vector2 p2, Vector2 p3, Vector2 p4, out Vector2 intersection )
		{
			intersection = Vector2.zero;


			float Ax,Bx,Cx,Ay,By,Cy,d,e,f,num/*,offset*/;

			float x1lo,x1hi,y1lo,y1hi;



			Ax = p2.x-p1.x;

			Bx = p3.x-p4.x;



			// X bound box test/

			if(Ax<0) {

				x1lo=p2.x; x1hi=p1.x;

			} else {

				x1hi=p2.x; x1lo=p1.x;

			}



			if(Bx>0) {

				if(x1hi < p4.x || p3.x < x1lo) return false;

			} else {

				if(x1hi < p3.x || p4.x < x1lo) return false;

			}



			Ay = p2.y-p1.y;

			By = p3.y-p4.y;



			// Y bound box test//

			if(Ay<0) {                  

				y1lo=p2.y; y1hi=p1.y;

			} else {

				y1hi=p2.y; y1lo=p1.y;

			}



			if(By>0) {

				if(y1hi < p4.y || p3.y < y1lo) return false;

			} else {

				if(y1hi < p3.y || p4.y < y1lo) return false;

			}



			Cx = p1.x-p3.x;

			Cy = p1.y-p3.y;

			d = By*Cx - Bx*Cy;  // alpha numerator//

			f = Ay*Bx - Ax*By;  // both denominator//



			// alpha tests//

			if(f>0) {

				if(d<0 || d>f) return false;

			} else {

				if(d>0 || d<f) return false;

			}



			e = Ax*Cy - Ay*Cx;  // beta numerator//



			// beta tests //

			if(f>0) {                          

				if(e<0 || e>f) return false;

			} else {

				if(e>0 || e<f) return false;

			}



			// check if they are parallel

			if(f==0) return false;

			// compute intersection coordinates //

			num = d*Ax; // numerator //

			//    offset = same_sign(num,f) ? f*0.5f : -f*0.5f;   // round direction //

			//    intersection.x = p1.x + (num+offset) / f;
			intersection.x = p1.x + num / f;



			num = d*Ay;

			//    offset = same_sign(num,f) ? f*0.5f : -f*0.5f;

			//    intersection.y = p1.y + (num+offset) / f;
			intersection.y = p1.y + num / f;



			return true;

		}




		public static bool isBetween(Vector2 a, Vector2 b, Vector2 c) {

			float crossproduct = (c.y - a.y) * (b.x - a.x) - (c.x - a.x) * (b.y - a.y);
			if (Mathf.Abs(crossproduct) > Mathf.Epsilon) {
				return false;
			}
			float dotproduct = (c.x - a.x) * (b.x - a.x) + (c.y - a.y) * (b.y - a.y);
			if (dotproduct < 0) {
				return false;
			}
			float squaredlengthba = (b.x - a.x)*(b.x - a.x) + (b.y - a.y)*(b.y - a.y);
			if (dotproduct > squaredlengthba) {
				return false;
			}
			return true;
		}





		public static float angleBetweenPoints(Vector3 p1, Vector3 p2) {
			float xDiff = p1.x - p2.x;
			float yDiff = p1.z - p2.z;
			float angle = (float)Math.Atan2(yDiff, xDiff) * (float)(180.0f / Math.PI);
			return Math.Abs(angle - 180.0f);
		}

		public static float lengthBetweenPoints(Vector3 a, Vector3 b)
		{
			return (float)( Math.Sqrt ( (a.x - b.x) * (a.x - b.x) + (a.z - b.z) * (a.z - b.z) ) );
		}


		public static Rect createRect(List<Vector3> r) {
			Rect rec = new Rect (r[0].x, r[0].z, r[1].x - r[0].x, r[2].z - r[1].z);
			return SRect.normalizeRect(rec);
		}

		public static void addGizmo(Vector3 pos, object message) {
			GameGizmo giz = new GameGizmo ();
			giz.pos = pos;
			giz.message = message;

			TextGizmo.Instance.gizmos.Add (giz);
		}

		public static void clearGizmos() {
			TextGizmo.Instance.gizmos.Clear ();
			TextGizmo.Instance.cubeGizmos.Clear ();
			TextGizmo.Instance.lines.Clear ();
		}

		public static void addCubeGizmo(Vector3 pos, Color color) {
			CubeGizmo g = new CubeGizmo ();
			g.color = color;
			g.pos = pos;
			TextGizmo.Instance.cubeGizmos.Add (g);	
		}

		public static Vector3 perpendicular(Vector3 a, Vector3 b, float f) {
			Vector3 dir = a - b; 
			Vector3 perpendicular = new Vector3(-dir.z, 0.0f, dir.x) / (float)Math.Sqrt(dir.x*dir.x + dir.z*dir.z) * 0.5f * f;
			return perpendicular;
		}
	}

	public class Furniture {
		protected List<Cell> occupied = new List<Cell>();
		protected List<Cell> mustBePassable = new List<Cell>();

		public Cell startCell;
		protected float angle;

		public static Furniture4x3 find4x3(List<Cell> cells, int angle = 0) {
			cells.Shuffle ();
			foreach (Cell cell in cells) {
				Furniture4x3 f = new Furniture4x3 (cell, angle);
				if (f.isAlreadyOccupied () == false && f.inside ()) {
					return f;
				}
			}

			return null;
		}

		public Furniture(Cell cell, float angle) {
			startCell = cell;
		}

		public bool inside() {
			if (startCell.parentRoom == null) {
				return false;
			}

			foreach(Cell cell in occupied) {
				if (cell.parentRoom != startCell.parentRoom)
					return false;
			}

			foreach(Cell cell in mustBePassable) {
				if (cell.parentRoom != startCell.parentRoom)
					return false;
			}
			return true;
		}

		public bool isAlreadyOccupied() {
			foreach(Cell cell in occupied) {
				if (cell.occupied == true || cell.mustBePassable == true) {
					return true;
				}
			}

			foreach(Cell cell in mustBePassable) {
				if (cell.occupied == true) {
					return true;
				}
			}
			return false;
		}

		public void occupy(bool value) {
			foreach(Cell cell in occupied) {
				cell.setOccupied (value);
			}

			foreach(Cell cell in mustBePassable) {
				cell.mustBePassable = value;
			}
		}

		public void drawGizmo() {
			foreach (Cell cell in occupied) {
				cell.drawCubeGizmo ();
			}
			foreach (Cell cell in mustBePassable) {
				cell.drawCubeGizmo ();
			}
		}

	}

	public class Furniture2x1 : Furniture {
		public Furniture2x1(Cell cell, float angle) : base(cell, angle) {
			if (angle == 0) {
				occupied.Add (cell);
				occupied.Add (cell.getCellToTheRight ());

				mustBePassable.Add (cell.getCellBelow ());
				mustBePassable.Add (cell.getCellBelow ().getCellToTheRight ());
				mustBePassable.Add (cell.getCellBelow ().getCellToTheRight ().getCellToTheRight ());
			} else if (angle == 90) {
				occupied.Add (cell);
				occupied.Add (cell.getCellBelow ());

				mustBePassable.Add (cell.getCellToTheLeft ());
				mustBePassable.Add (cell.getCellBelow ().getCellToTheLeft ());
				mustBePassable.Add (cell.getCellBelow ().getCellBelow ().getCellToTheLeft ());
			} else if (angle == 180) {
				occupied.Add (cell);
				occupied.Add (cell.getCellToTheLeft ());

				mustBePassable.Add (cell.getCellAbove());
				mustBePassable.Add (cell.getCellToTheLeft().getCellAbove());
				mustBePassable.Add (cell.getCellToTheLeft().getCellToTheLeft().getCellAbove());
			}
		}
	}

	public class Furniture4x3 : Furniture {
		public Furniture4x3(Cell cell, float angle) : base(cell, angle) {
			occupied.Add (cell);

			mustBePassable.Add(cell.getCellAbove ());

			int w = 4;
			int h = 3;

			Cell cv = cell.getCellAbove ();
			for(int j = 0; j < h; j++) {
				cv = cv.getCellBelow ();
				Cell ch = cv;
				for (int i = 0; i < w; i++) {
					ch = ch.getCellInDirectionHorizontal (i);
					occupied.Add (ch);
				}
			}
		}

	}





	public class Segment {
		public bool intersect = false;
		public bool skip = false;
        public bool noDoor = false;

		public int i = 0;
		public int j = 0;

		public Vector3 pos;
		public Vector3 center;

		public Wall wall;

		public GameObject wallObj;

		public bool isHorizontal() {
			return (pos.z % 1 == 0);
		}

		public Segment() {
		}

		public Cell getCell() {

			return this.wall.getRoom().floor.grid.getCellByPos (center);
		}

		public Grid getGrid() {
			return this.wall.getRoom ().floor.grid;
		}

		public Cell getCellAbove() {			
			return getGrid().getCellByPos (center + new Vector3 (0, 0, 0.1f));
		}

		public Cell getCellBelow() {
            return getGrid().getCellByPos(center + new Vector3(0, 0, -0.1f));
            //return getCellAbove ().getCellBelow ();
        }

		public Cell getCellRight() {			
			return getGrid().getCellByPos (center + new Vector3 (0.1f, 0, 0));
		}

		public Cell getCellLeft() {
            return getGrid().getCellByPos(center + new Vector3(-0.1f, 0, 0));
            //return getCellRight ().getCellToTheLeft ();
        }
	}


	public class Wall {

		public static int BOTTOM = 1;
		public static int RIGHT = 2;
		public static int TOP = 3;
		public static int LEFT = 4;

		private Room room = null;
		int index1;
		int index2;

		Vector3 p1; 
		Vector3 p2;

		public int direction;

		public ListSegment segments = new ListSegment ();

		public float length;

		public Wall(Room room, int index1, int index2, int direction) {
			this.room = room;
			this.index1 = index1;
			this.index2 = index2;
			this.direction = direction;

			length = (int)Util.lengthBetweenPoints (room.getPoint (index1), room.getPoint (index2));

		}


		public Room getRoom() {
			if (room == null) {
				throw new System.ArgumentException ("Room is null", "room");
			}
			return this.room;
		}

		public void createSegments() {
			if (room != null) {
				this.p1 = getP1 ();
				this.p2 = getP2 ();
			};


			length = Util.lengthBetweenPoints (p1, p2);

			for (int i = 0; i < length / Util.CELL_SIZE; i++) {
				Segment seg = new Segment();
				seg.wall = this;

				if (direction == Wall.BOTTOM) {
					seg.pos = p1 + new Vector3 (i, 0, 0) * Util.CELL_SIZE;
					seg.center = p1 + new Vector3 (i, 0, 0) * Util.CELL_SIZE + new Vector3 (Util.CELL_SIZE, 0, 0) / 2f;
					seg.i = i;
					seg.j = 0;

				} else if (direction == Wall.RIGHT) {
					seg.pos = p1 + new Vector3 (0, 0, i) * Util.CELL_SIZE;
					seg.center = p1 + new Vector3 (0, 0, i) * Util.CELL_SIZE + new Vector3(0, 0, Util.CELL_SIZE) / 2f;
					seg.i = room.getWidth ();
					seg.j = i;

				} else if (direction == Wall.TOP) {
					seg.pos = p1 - new Vector3 (i, 0, 0) * Util.CELL_SIZE;
					seg.center = p1 - new Vector3 (i, 0, 0) * Util.CELL_SIZE - new Vector3(Util.CELL_SIZE, 0, 0) / 2f;
					seg.i = room.getWidth() - i - 1;
					seg.j = room.getHeight() - 1;

				} else if (direction == Wall.LEFT) {
					seg.pos = p1 - new Vector3 (0, 0, i) * Util.CELL_SIZE;
					seg.center = p1 - new Vector3 (0, 0, i) * Util.CELL_SIZE - new Vector3(0, 0, Util.CELL_SIZE) / 2f;
					seg.i = 0;
					seg.j = room.getHeight() - i - 1;
				}


				//Util.addGizmo (seg.center, this.room.number);

				segments.Add(seg);
			}
		}


		public List<Room> getIntersectRooms() {
			List<Room> roomIntersects = new List<Room> ();
			foreach (Segment seg in segments) {
				Room r = null;
				if (direction == Wall.BOTTOM) {
					r = seg.getCell().getCellBelow ().parentRoom;
				}
				if (direction == Wall.RIGHT) {
					r = seg.getCell ().getCellToTheRight ().parentRoom;
				}
				if (direction == Wall.TOP) {
					r = seg.getCell ().getCellAbove ().parentRoom;
				}
				if (direction == Wall.LEFT) {
					r = seg.getCell ().getCellToTheLeft ().parentRoom;
				}

				if (r != null && !roomIntersects.Contains (r)) {
					roomIntersects.Add (r);
				}
			}
			return roomIntersects;
		}

		public void setIntersection(int index, bool value) {
			this.segments [index].intersect = value;
		}

		public GameObject createWall() {
			float z = room.grid.height;
			return createWall (0);
		}

		public Vector3 getP1() {
			return room.getPoint(index1);
		}

		public Vector3 getP2() {
			return room.getPoint(index2);
		}

		public GameObject createWall(float z) {
			float angle = Util.angleBetweenPoints (p1, p2);


			GameObject wallFull = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Generator/WallFull"));
			wallFull.transform.position = new Vector3 (0, 0, 0);
			wallFull.layer = LayerMask.NameToLayer ("Obstacle");


			List<GameObject> walls = new List<GameObject> ();
			for (int i = 0; i < length / Util.CELL_SIZE; i++) {
				if (segments[i].intersect) {
					if (direction == Wall.TOP) {
                        continue;
                    }

                    if (direction == Wall.LEFT) {
                        continue;
                    }
                }
				if (segments[i].skip) {
					continue;
				}
				GameObject wall = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Generator/Wall"));

				wall.transform.position = new Vector3 (Util.CELL_SIZE * i + Util.CELL_SIZE / 2.0f, z, 0.0f);
				wall.transform.parent = wallFull.transform;


				segments [i].wallObj = wall;

				walls.Add (wall);
			}

			wallFull.transform.position = p1;
			wallFull.transform.Rotate (0.0f, angle, 0.0f);


			foreach (GameObject wall in walls) {
				wall.transform.parent = GameObject.Find ("Level1").transform;
			}
			//wallFull.transform.parent = GameObject.Find ("Level1").transform;
			GameObject.Destroy(wallFull);

			//Debug.Log (wall.GetComponent<Renderer>().bounds.size);

			return wallFull;
		}

	}


	public class Cell {
		public Rect rect;
		float x;
		float z;
		float y;

		public Room parentRoom;

		public int localI;
		public int localJ;

		public bool occupied = false;
		public bool mustBePassable = false;

		public int worldI;
		public int worldJ;

		public Grid grid;

		public Cell(int i, int j, Grid grid) {
			worldI = i;
			worldJ = j;
			this.grid = grid;

			this.x = worldI * Util.CELL_SIZE;
			this.z = worldJ * Util.CELL_SIZE;
			this.y = grid.height;

			rect = new Rect (this.x, this.z, Util.CELL_SIZE, Util.CELL_SIZE);
		}

		public Cell getCellInDirectionHorizontal(int step) {
			if (step == 0) {
				return this;
			}
			if (step > 1) {
				return getCellInDirectionHorizontal (step - 1);
			}
			if (step == 1) {
				return getCellToTheRight ();
			}

			if (step < -1) {
				return getCellInDirectionHorizontal (step + 1);
			}
			if (step == -1) {
				return getCellToTheLeft ();
			}

			throw new Exception ("Return a null.");
			return null;
		}

		/*
		public Cell(float x, float z) {
			worldI = (int)(x) / (int)Util.CELL_SIZE;
			worldJ = (int)(z) / (int)Util.CELL_SIZE;

			this.x = worldI * Util.CELL_SIZE;
			this.z = worldJ * Util.CELL_SIZE;

			rect = new Rect (this.x, this.z, Util.CELL_SIZE, Util.CELL_SIZE);
		}
		*/

		public Vector3 getWorldPosition() {
			return getWorldPosition (0, 0);
		}

		public Vector3 getWorldPosition(float dx, float dz) {
			return new Vector3(rect.center.x-Util.CELL_SIZE/2.0f, this.y, rect.center.y-Util.CELL_SIZE/2.0f) +
				new Vector3(dx, 0, dz);
		}

		public void drawGizmo() {
			Util.addGizmo (getWorldPosition(), String.Format ("({0},{1})", this.worldI, this.worldJ));
		}

		public void drawCubeGizmo() {
			Util.addCubeGizmo (getWorldPosition (0.5f, 0.5f), new Color(1, 0, 0, 0.5F));
		}

        public void drawCubeGizmo(Color color) {
            Util.addCubeGizmo(getWorldPosition(0.5f, 0.5f), color);
        }

        public void setOccupied(bool val) {
			occupied = val;
		}

		public Cell getCellAbove() {
			return grid.getCellByPos (getWorldPosition()+new Vector3 (0, 0, Util.CELL_SIZE));
		}
		public Cell getCellBelow() {
			return grid.getCellByPos (getWorldPosition()+new Vector3 (0, 0, -Util.CELL_SIZE));
		}
		public Cell getCellToTheRight() {
			return grid.getCellByPos (getWorldPosition()+new Vector3 (Util.CELL_SIZE, 0, 0));
		}
		public Cell getCellToTheLeft() {
			return grid.getCellByPos (getWorldPosition()+new Vector3 (-Util.CELL_SIZE, 0, 0));
		}

	}

	/*
	public class FloorCell : Cell {
		
		public FloorCell(float roomX, float roomZ, int i, int j) : base (roomX + i * Util.CELL_SIZE, roomZ + j * Util.CELL_SIZE) {
			
		}

	}
	*/


	public class Space {
		public float x;
		public float y;
		public float z;

		public float width;
		public float height;
		public float level;

		public Grid grid;

		public List<bool> freeDirections = new List<bool>() {true, true, true, true};


		public Space(Vector3 p1, Vector3 p3, Grid grid) {
			this.grid = grid;
			initialize(p1.x, p1.y, p1.z, p3.x-p1.x, p3.z-p1.z, grid);
		}

		public void initialize(float x, float y, float z, float width, float height, Grid grid) {
			this.x = x;
			this.y = y;
			this.z = z;
			this.width = width;
			this.height = height;
			this.grid = grid;
		}


		public Cell getCell(int i, int j) {
			return grid.getCellByPos (x + i * Util.CELL_SIZE, z + j * Util.CELL_SIZE);
		}

		public GameObject draw() {
			Rect rect = new Rect (x, z, width, height);

			GameObject plane = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Generator/Floor"));
			//GameObject plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
			plane.transform.position = new Vector3(rect.center.x, this.y + 0.01F, rect.center.y);
			plane.transform.Rotate (0, 0, 0);

			Renderer renderer = plane.GetComponent<Renderer>();

			float eX = renderer.bounds.extents.x;
			float scaleX = (float)width / eX * 2.0f * 0.25f;
			float eY = eX;
			float scaleY = (float)height / eY * 2.0f * 0.25f;
			plane.transform.localScale = new Vector3(scaleX, 0.1f, scaleY);

			renderer.material = Util.floor_material[ UnityEngine.Random.Range(0, Util.floor_material.Count) ];


			plane.transform.parent = GameObject.Find ("Level1").transform;

			return plane;
		}
	}

	public class Floor : Space {
		GameObject plane;
		public List<Cell> exits = new List<Cell>();

		public Floor(Vector3 p1, Vector3 p3, Grid grid) : base (p1, p3, grid) {
		}

		public Cell getCell(int i, int j) {
			Cell cell = base.getCell (i, j);
			cell.localI = i;
			cell.localJ = j;
			return cell;
		}





		public IEnumerable getCells() {
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					yield return getCell (i, j);
				}
			}
			yield break;
		}

		public List<Cell> getCellsList() {
			List<Cell> cells = new List<Cell> ();
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {
					cells.Add (getCell (i, j));
				}
			}
			return cells;
		}

		public bool isPassable() {
			for (int i = 0; i < width; i++) {
				for (int j = 0; j < height; j++) {

					Cell a = getCell (i, j);
					if (a.mustBePassable == false) {
						continue;
					}

					for (int m = 0; m < width; m++) {
						for (int n = 0; n < height; n++) {
							Cell b = getCell (m, n);

							if (b.mustBePassable == false) {
								continue;
							}

							if (i == m && j == n) {
								continue;
							}

							if (findPath (a, b) == null) {
								return false;
							}
						}
					}

				}
			}
			return true;
		}


		public List<Cell> findPath(Cell enter, Cell exit) {
			List<Cell> path = new List<Cell> ();
			path.Add (enter);

			if (enter.occupied || exit.occupied) {
				return null;
			}

			return _findPath (path, exit);
		}

		public List<Cell> _findPath(List<Cell> path, Cell exit) {
            if (path.Count > 15) return null;

            List<List<Cell>> paths = new List<List<Cell>>();

			List<Cell> dirs = getPathDirections (path.Last());
			foreach (Cell dir in dirs) {
				if (path.Contains (dir)) {
					continue;
				}
				if (dir.occupied) {
					continue;
				}


				List<Cell> newPath = path.ToList();
				newPath.Add (dir);

				if (dir == exit) {
					paths.Add(newPath);
                    break;
				}

				List<Cell> pathFound = _findPath (newPath, exit);
				if (pathFound != null && pathFound.Last() == exit) {
                    paths.Add(pathFound);
				}
                
			}

            if (paths.Count > 0)
                return paths.OrderBy(p => p.Count).ToList()[0];

			return null;
		}


		private List<Cell> getPathDirections(Cell c) {
			List<Cell> dirs = new List<Cell> ();

            Cell cA = c.getCellAbove ();
			if (c.parentRoom == cA.parentRoom) {
				dirs.Add (cA);
			}

			Cell cR = c.getCellToTheRight ();
			if (c.parentRoom == cR.parentRoom) {
				dirs.Add (cR);
			}

			Cell cB  = c.getCellBelow ();
			if (c.parentRoom == cB.parentRoom) {
				dirs.Add (cB);
			}

			Cell cL = c.getCellToTheLeft ();
			if (c.parentRoom == cL.parentRoom) {
				dirs.Add (cL);
			}


			return dirs;
		}

	}


	public static class RoomType
	{
		public static int HALL = 1;
		public static int OFFICE = 2;
        public static int CORRIDOR = 3;
	}

	public class PolylineRoom : Room 
	{
		List<Vector3> vertices;



		public PolylineRoom(List<Vector3> vertices, Grid grid) {
			this.vertices = vertices;
			this.grid = grid;

			create ();
		}

		private void create() {
			this.number = createUniqueId ();


			for (int i = 0; i < vertices.Count; i++) {
				Vector3 v = vertices[i];
				this.Add (new Vector3 (v.x, grid.height, v.z));
			}

			walls.Clear ();

			for (int i = 0; i < vertices.Count; i++) {
				int next = Util._index (i + 1, vertices.Count);
				walls.Add (new Wall (this, i, next, Wall.BOTTOM));
			}

		}

		public void createFloor() {
		}
	}

	public class Room : List<Vector3>
	{
        public List<RoomConnection> _doorPositions = new List<RoomConnection>();
        public List<GameObject> doors = new List<GameObject>();

        public bool noDoors = false;
        public Room ownedBy = null;

		public int number;

		public int roomType = 0;
		public List<Wall> walls = new List<Wall> ();
		public Floor floor;
		public Floor roof;

		public Grid grid;

        public int x;
        public int y;
        public int width;
        public int height;

        

		/*
		public Room(IEnumerable<Vector3> coords) : base(coords) {

		} 
		*/
		protected Room() {

		}

        public Room(float x, float y, float width, float height) : base() {
            create(x, y, width, height);
        }

        public Room(float x, float y, float width, float height, Grid grid) : base() {
			create (x, y, width, height, grid);
		} 
        
        public Room(Rect r) {
            create(r.x, r.y, r.width, r.height);
        }

		public Room(Rect r, Grid grid) {
			create (r.x, r.y, r.width, r.height, grid);	
		}

		protected int createUniqueId() {
			Room._roomNumber++;

			return Room._roomNumber;
		}

		private void create(float x, float y, float width, float height, Grid grid = null) {
            this.x = (int)x;
            this.y = (int)y;
            this.width = (int)width;
            this.height = (int)height;

            float gridHeight = 0;
            if (grid != null) gridHeight = grid.height;

			this.Add (new Vector3 (x, gridHeight, y));
			this.Add (new Vector3 (x + width, gridHeight, y));
			this.Add (new Vector3 (x + width, gridHeight, y + height));
			this.Add (new Vector3 (x, gridHeight, y + height));

			this.grid = grid;

			this.number = createUniqueId ();

			walls.Clear ();
			walls.Add (new Wall (this, 0, 1, Wall.BOTTOM));
			walls.Add (new Wall (this, 1, 2, Wall.RIGHT));
			walls.Add (new Wall (this, 2, 3, Wall.TOP));
			walls.Add (new Wall (this, 3, 0, Wall.LEFT));
		}

        public void SetGrid(Grid grid) {
            this.grid = grid;
            for(int i = 0; i < this.Count; i++) {
                this[i].Set(this[i].x, grid.height, this[i].z);
            }
        }

		public Vector3 getPoint(int index) {
			return this [index];
		}

		public Rect getRectangle() {
			return new Rect (this [0].x, this [0].z, getWidth (), getHeight ());
		}


		public Wall getBottomWall() {
			return walls[0];
		}

		public Wall getRightWall() {
			return walls [1];
		}

		public Wall getTopWall() {
			return walls[2];
		}

		public Wall getLeftWall() {
			return walls [3];
		}

        public ListSegment getLeftWallSegments() {
            return getLeftRoom().getRightWall().segments;
        }

        public ListSegment getRightWallSegments() {
            return getRightWall().segments;
        }

        public ListSegment getTopWallSegments() {
            return getTopRoom().getBottomWall().segments;
        }

        public ListSegment getBottomWallSegments() {
            return getBottomWall().segments;
        }

		public void translate(float dX, float dZ) {
			for (int i = 0; i < this.Count; i++) {
				this[i] = new Vector3 (this[i].x + dX, 0.0f, this[i].z + dZ);
			}
		}

		public void drawWalls() {
			for (int i = 0; i < walls.Count; i++) {
				walls[i].createWall();
			}
		}

		public void createFloor() {
			this.floor = new Floor (this[0], this[2], this.grid);

			for (int i = 0; i < floor.width; i++) {
				for (int j = 0; j < floor.height; j++) {
					floor.getCell (i, j).parentRoom = this;
				}
			}

			//this.roof = new Floor (this[0] + new Vector3(0, 2f, 0), this[2] + new Vector3(0, 2f, 0));
		}

		public void createSegments() {
			foreach (Wall wall in walls) {
				wall.createSegments ();
			}
		}

		public void drawFloor() {
			this.floor.draw ();

			//GameObject roofObj = this.roof.draw ();

		}

		public int getWidth() {
			return (int)Util.lengthBetweenPoints (this [0], this [1]);
		}

		public int getHeight() {
			return (int)((this [2].z - this [0].z) / Util.CELL_SIZE);
		}

		public Vector3 getRoomCenter() {
			return new Vector3((this[0].x + (this[1].x - this[0].x) / 2.0f), 0, 
				(this[0].z + (this[3].z - this[0].z) / 2.0f));
		}

		public ListSegment getRoomSegments() {
			ListSegment segments = new ListSegment(this.getLeftWall ().segments
				.Concat (this.getRightWall ().segments)
				.Concat (this.getTopWall ().segments)
				.Concat (this.getBottomWall ().segments));
			return segments;
		}

		public List<Room> getIntersectRooms() {
			return new List<Room>(getBottomWall ().getIntersectRooms ()
				.Concat(getRightWall ().getIntersectRooms ())
				.Concat(getTopWall ().getIntersectRooms ())
				.Concat(getLeftWall ().getIntersectRooms ()));
		}


		private static int _roomNumber = 0;



		public static Room createRandomRoom(Grid grid) {
			int mapWidth = 2;
			int mapHeight = 2;

			int minRoomX = 4;
			int minRoomY = 4;
			int maxRoomX = 12;
			int maxRoomY = 12;




			int x = Random.Range (0, mapWidth);
			int y = Random.Range (0, mapHeight);

			int width = Random.Range (minRoomX, maxRoomX);
			int height = Random.Range (minRoomY, maxRoomY);
			float ratio = width / height;
			if (ratio < 0.5 || ratio > 1.5) {
				return createRandomRoom (grid);
			}

			if (width % 2 == 1 || height % 2 == 1) {
				return createRandomRoom (grid);
			}
			if (x % 2 == 1 || y % 2 == 1) {
				return createRandomRoom (grid);
			}

			Room r1 = new Room (x, y, width, height, grid);
			r1.roomType = 0;

			Room._roomNumber++;
			return r1;
		}


        public Room getLeftRoom() {
            return getLeftWall().segments[0].getCellLeft().parentRoom;
        }

        public Room getRightRoom() {
            return getRightWall().segments[0].getCellRight().parentRoom;
        }

        public Room getTopRoom() {
            return getTopWall().segments[0].getCellAbove().parentRoom;
        }

        public Room getBottomRoom() {
            return getBottomWall().segments[0].getCellBelow().parentRoom;
        }

    }


	public class ListSegment : List<Segment> {
		public ListSegment(IEnumerable<Segment> segments) : base(segments) {} 
		public ListSegment() : base() {} 

        public void Set(int i, ref Segment seg) {
            this[i] = seg;
        }


		public ListSegment getIntersectSegments() {
			ListSegment intersect = new ListSegment ();
			foreach (Segment seg in this) {
				if (seg.intersect) {
					intersect.Add (seg);
				}
			}
			return intersect;
		}

		public ListSegment getNonIntersectSegments() {
			ListSegment intersect = new ListSegment ();
			foreach (Segment seg in this) {
				if (seg.intersect == false) {
					intersect.Add (seg);
				}
			}
			return intersect;
		}

        public ListSegment getNotSkipSegments() {
            ListSegment nonSkip = new ListSegment();
            foreach(Segment seg in this) {
                if (seg.skip == false && seg.noDoor == false) {
                    nonSkip.Add(seg);
                }
            }
            return nonSkip;
        }
	}



	public class ListRoom {
		public List<Room> rooms;

        public ListRoom() {
            this.rooms = new List<Room>();
        }

		public ListRoom(List<Room> rooms) {
			this.rooms = rooms;
		}

		public ListSegment getAllSegments() {
			ListSegment segs = new ListSegment ();
			for (int i = 0; i < rooms.Count; i++) {
				segs = new ListSegment(segs.Concat (rooms [i].getRoomSegments ()));
			}

			return segs;
		}

        public Room getRoomByNumber(int number) {
            foreach(Room room in rooms) {
                if (room.number == number) {
                    return room;
                }
            }
            return null;
        }
	}

	public class WindowGenerator {
		public ListRoom rooms;
		public WindowGenerator(ListRoom rooms){
			this.rooms = rooms;
		}

		public void makeWindows() {
			ListSegment segs = rooms.getAllSegments ();
			ListSegment nonIntersect = segs.getNonIntersectSegments ();

			int count = 0;
			foreach (Segment seg in nonIntersect) {
				if (count % 3 == 0 && !seg.skip) {
					seg.skip = true;					

					GameObject window = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Generator/Window"));
					place (seg, window);
				}
				count++;
			}
			/*
			int count = Random.Range (1, 4);
			for (int i = 0; i < count; i++) {
				Segment seg = nonIntersect.SelectRandom ();
				seg.skip = true;

				//
			}
			*/

		}

		public void place(Segment seg, GameObject obj) {
			obj.transform.position = seg.pos;
			obj.layer = LayerMask.NameToLayer ("Obstacle");

			if (seg.wall.direction == Wall.BOTTOM) {
				obj.transform.position += new Vector3 (0.5f, 0, 0f);
			} else if (seg.wall.direction == Wall.RIGHT) {
				obj.transform.rotation = Quaternion.Euler (0, -90, 0);
				obj.transform.position += new Vector3 (0, 0, 0.5f);
			} else if (seg.wall.direction == Wall.TOP) {
				obj.transform.rotation = Quaternion.Euler (0, 180, 0);
				obj.transform.position -= new Vector3 (0.5f, 0, 0f);
			} else if (seg.wall.direction == Wall.LEFT) {
				obj.transform.rotation = Quaternion.Euler (0, 90, 0);
				obj.transform.position -= new Vector3 (0, 0, 0.5f);
			}
			obj.transform.parent = GameObject.Find ("Level1").transform;
		}
	}


	public class DoorGenerator {
		public List<Room> rooms;
        public Grid grid;
		public DoorGenerator(List<Room> rooms, Grid grid) {
			this.rooms = rooms;
            this.grid = grid;
		}

		public void makeDoors() {
            makeConnectionDoors();
            //makeInnerDoors ();
			makeOuterDoors ();
			makePath ();

            
		}

        public void makeConnectionDoors() {
            foreach (Room room in rooms) {
                if (room._doorPositions.Count > 0) {
                    room._doorPositions = room._doorPositions.OrderBy((item) => Random.Range(0, 1000)).ToList();

                    foreach (RoomConnection connection in room._doorPositions) {
                        Point p = connection.connection;
                        Cell c = grid.getCellByPos(p.x, p.y);
                        if (c.getCellBelow().parentRoom == connection.room) {
                            Segment seg = room.getBottomWallSegments()[p.x - room.x];
                            seg.skip = true;
                            GameObject door = placeDoor(seg);
                            room.doors.Add(door);
                            connection.room.doors.Add(door);

                            break;
                        }
                    }

                    foreach (RoomConnection connection in room._doorPositions) {
                        Point p = connection.connection;
                        Cell c = grid.getCellByPos(p.x, p.y);
                        if (c.getCellAbove().parentRoom == connection.room) {                            
                            Segment seg = room.getTopWall().segments[0];
                            seg.skip = true;
                            GameObject door = placeDoor(seg);
                            room.doors.Add(door);
                            connection.room.doors.Add(door);

                            break;
                        }
                    }

                    foreach (RoomConnection connection in room._doorPositions) {
                        Point p = connection.connection;
                        Cell c = grid.getCellByPos(p.x, p.y);
                        if (c.getCellToTheRight().parentRoom == connection.room) {
                            Segment seg = room.getRightWall().segments[p.y - room.y];
                            seg.skip = true;
                            GameObject door = placeDoor(seg);
                            room.doors.Add(door);
                            connection.room.doors.Add(door);

                            break;
                        }
                    }

                    foreach (RoomConnection connection in room._doorPositions) {
                        Point p = connection.connection;
                        Cell c = grid.getCellByPos(p.x, p.y);
                        if (c.getCellToTheLeft().parentRoom == connection.room) {
                            Segment seg = room.getLeftWall().segments[(room.height - 1) - (p.y - room.y)];
                            seg.skip = true;
                            GameObject door = placeDoor(seg);
                            room.doors.Add(door);
                            connection.room.doors.Add(door);

                            break;
                        }
                    }
                }
            }
        }

		public GameObject placeDoor(Segment seg) {
            GameObject obj = (GameObject)UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Generator/DoorHinge"));

            obj.transform.position = seg.pos;
			obj.layer = LayerMask.NameToLayer ("Obstacle");

			if (seg.wall.direction == Wall.BOTTOM) {
                seg.getCellAbove().mustBePassable = true;
                seg.getCellBelow().mustBePassable = true;
                if (seg.getCellAbove().parentRoom != null) seg.getCellAbove().parentRoom.floor.exits.Add(seg.getCellAbove());                
                if (seg.getCellBelow().parentRoom != null) seg.getCellBelow().parentRoom.floor.exits.Add(seg.getCellBelow());

                //seg.getCellAbove().drawCubeGizmo();
                //seg.getCellBelow().drawCubeGizmo();

            } else if (seg.wall.direction == Wall.RIGHT) {
				obj.transform.rotation = Quaternion.Euler (0, 90, 0);
				obj.transform.position += new Vector3 (0f, 0, 1f);

                seg.getCellLeft().mustBePassable = true;
                seg.getCellRight().mustBePassable = true;                
                if (seg.getCellLeft().parentRoom != null) seg.getCellLeft().parentRoom.floor.exits.Add(seg.getCellLeft());
                if (seg.getCellRight().parentRoom != null) seg.getCellRight().parentRoom.floor.exits.Add(seg.getCellRight());

                //seg.getCellRight().drawCubeGizmo();
                //seg.getCellLeft().drawCubeGizmo();

            } else if (seg.wall.direction == Wall.TOP) {
				obj.transform.rotation = Quaternion.Euler (0, 180, 0);


			} else if (seg.wall.direction == Wall.LEFT) {
				obj.transform.rotation = Quaternion.Euler (0, 90, 0);
			}
			obj.transform.parent = GameObject.Find ("Level1").transform;

            return obj;
		}

		public void makeInnerDoors() {
			for (int i = 0; i < rooms.Count; i++) {
                if (rooms[i].roomType != RoomType.HALL) continue;

				ListSegment segments;

				Grid grid = rooms[i].floor.grid;

				segments = rooms [i].getBottomWall ().segments.getIntersectSegments ().getNotSkipSegments();
				if (segments.Count > 0) {
					Segment seg = segments.SelectRandom ();
					seg.skip = true;
                    
					placeDoor (seg);
				}

				segments = rooms [i].getRightWall ().segments.getIntersectSegments ().getNotSkipSegments();
				if (segments.Count > 0) {
					Segment seg = segments.SelectRandom ();
					seg.skip = true;
                    
					placeDoor (seg);
				}
			}
		}

		public void makeOuterDoors() {			
			ListSegment segs = new ListSegment ();
			for (int i = 0; i < rooms.Count; i++) {
                if (rooms[i].roomType == RoomType.HALL) {
                    segs = new ListSegment(segs.Concat(rooms[i].getRoomSegments()).ToList());
                }
			}

			int count = Random.Range (1, 1);
			ListSegment nonIntersect = segs.getNonIntersectSegments ().getNotSkipSegments();
			for (int i = 0; i < count; i++) {
				Segment seg = nonIntersect.SelectRandom ();
				seg.skip = true;
                
				placeDoor (seg);
			}
		}

		public void makePath() {
			foreach (Room room in rooms) {
				foreach (Cell exit in room.floor.exits) {
					if (exit == room.floor.exits [0])
						continue;

					List<Cell> path = room.floor.findPath (room.floor.exits [0], exit);
					if (path == null) {
						continue;
					}

                    Color color = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 0.5f);
					foreach (Cell cell in path) {
						cell.mustBePassable = true;
                        cell.drawCubeGizmo(color);
					}

				}
			}
		}
	}
}