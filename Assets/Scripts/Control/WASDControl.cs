﻿using UnityEngine;
using System.Collections;

public class WASDControl : MonoBehaviour {
	public Animator Anim;
	public CharacterController cc;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate() {
		float h = Input.GetAxis("Horizontal");
		float v = Input.GetAxis("Vertical");
		MoventManagement(h, v);
	}

	void MoventManagement(float horizontal, float vertical) {
		Anim.SetFloat ("Speed", vertical);
		
		transform.Rotate(new Vector3(0, horizontal * Time.deltaTime * 120,0));
		
		cc.SimpleMove (Physics.gravity);
		Vector3 forward = vertical * transform.TransformDirection (Vector3.forward) * 2;
		cc.Move (forward * Time.deltaTime);
	}
}
