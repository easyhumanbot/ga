﻿using UnityEngine;
using System.Collections;

public class RtsCam : MonoBehaviour
{
	public int LevelArea = 100;
	
	public int ScrollArea = 25;
	public int ScrollSpeed = 25;
	public int DragSpeed = 100;
	public float KeySpeed = 0.01f;
	
	public int ZoomSpeed = 25;
	public int ZoomMin = 20;
	public int ZoomMax = 100;
	
	public int PanSpeed = 50;
	public int PanAngleMin = 25;
	public int PanAngleMax = 80;


	public float minX = -360.0f;
	public float maxX = 360.0f;
	
	public float minY = -45.0f;
	public float maxY = 45.0f;
	
	public float sensX = 5000.0f;
	public float sensY = 5000.0f;
	
	float rotationY = 0.0f;
	float rotationX = 0.0f;


	private float speed = 2.0f;

    private int cameraMode = 0;


	private float zoomSpeed = 2.0f;
	void Update()
	{
		
		var flySpeed = 0.2f;
		var translation = Vector3.zero;
		
		
		// Zoom in or out
		var zoomDelta = Input.GetAxis("Mouse ScrollWheel")*ZoomSpeed*Time.unscaledDeltaTime;
		if (zoomDelta!=0)
		{
			translation -= Vector3.up * ZoomSpeed * zoomDelta;
		}


        if (Input.GetKeyDown(KeyCode.P)) {
            if (cameraMode == 0)
                cameraMode = 1;
            else
                cameraMode = 0;
        }

		if (Input.GetAxisRaw("Vertical") != 0)
		{
            if (cameraMode == 0)
			    transform.Translate(Vector3.up * flySpeed * Input.GetAxisRaw("Vertical"));
            else 
                transform.Translate(Vector3.forward * flySpeed * Input.GetAxisRaw("Vertical"));
        }
		
		if (Input.GetAxisRaw("Horizontal") != 0)
		{
			transform.Translate(Vector3.right * flySpeed * Input.GetAxisRaw("Horizontal"));
		}


		// Move camera with mouse
		/*
		if (Input.GetMouseButton(2)) // MMB
		{
			// Hold button and drag camera around
			translation -= new Vector3(Input.GetAxis("Mouse X") * DragSpeed * Time.deltaTime, 0, 
			                           Input.GetAxis("Mouse Y") * DragSpeed * Time.deltaTime);
		}
		else
		{
			// Move camera if mouse pointer reaches screen borders
			if (Input.mousePosition.x < ScrollArea)
			{
				translation += Vector3.right * -ScrollSpeed * Time.deltaTime;
			}
			
			if (Input.mousePosition.x >= Screen.width - ScrollArea)
			{
				translation += Vector3.right * ScrollSpeed * Time.deltaTime;
			}
			
			if (Input.mousePosition.y < ScrollArea)
			{
				translation += Vector3.forward * -ScrollSpeed * Time.deltaTime;
			}
			
			if (Input.mousePosition.y > Screen.height - ScrollArea)
			{
				translation += Vector3.forward * ScrollSpeed * Time.deltaTime;
			}
		}
		*/
		
		// Keep camera within level and zoom area
		/*
		var desiredPosition = GetComponent<Camera>().transform.position + translation;
		if (desiredPosition.x < -LevelArea || LevelArea < desiredPosition.x)
		{
			translation.x = 0;
		}
		if (desiredPosition.y < ZoomMin || ZoomMax < desiredPosition.y)
		{
			translation.y = 0;
		}
		if (desiredPosition.z < -LevelArea || LevelArea < desiredPosition.z)
		{
			translation.z = 0;
		}
		*/
		GetComponent<Camera>().transform.position += translation;

		Screen.lockCursor = false;
		if (Input.GetMouseButton (2)) {
			Screen.lockCursor = true;
			rotationX += Input.GetAxis ("Mouse X") * sensX * Time.unscaledDeltaTime;
			rotationY += Input.GetAxis ("Mouse Y") * sensY * Time.unscaledDeltaTime;
			//rotationY = Mathf.Clamp (rotationY, minY, maxY);
			transform.localEulerAngles = new Vector3 (-rotationY, rotationX, 0);
		}
	}
}