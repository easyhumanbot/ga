﻿using UnityEngine;
using System.Collections;

public class FireBullet : MonoBehaviour {
	private Ray ray; 
	private RaycastHit hit;
	public CharacterController cc;
	public GameObject gun;
	public ParticleSystem weaponFireParticles;
	public Light weaponFireLight;


	public GameObject bulletHolePrefab;
	void Start () {
		cc = GetComponent<CharacterController>();
	}



	public void PlayParticle ()
	{
		weaponFireParticles.Play ();
	}

	void Fire() {
		PlayParticle();

		GetComponent<AudioSource>().Play ();

		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition); 
		RaycastHit hit; 
		Physics.Raycast (ray, out hit, 1000);
		Vector3 clickedPosition = hit.point; 

		ray = new Ray (gun.transform.position, clickedPosition - gun.transform.position);

		if(Physics.Raycast(ray, out hit, Camera.main.farClipPlane)) 
		{
			Vector3 bulletHolePosition = hit.point + hit.normal * 0.01f;

			Quaternion bulletHoleRotation = Quaternion.FromToRotation(-Vector3.forward, hit.normal);				
			GameObject hole = (GameObject)GameObject.Instantiate(bulletHolePrefab, bulletHolePosition, bulletHoleRotation);
		}
	}

	void Update () {
		weaponFireLight.enabled = weaponFireParticles.isPlaying;

		if(Input.GetMouseButtonDown (0))
		{
			//Fire ();
		}
	}
}
